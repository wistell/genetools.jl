
function GeneTools.plot_diagnostic(diag::GeneTools.DiagControl,
                                   contours::GeneTools.Contours{T, S, F},
                                   spec::Symbol;
                                  ) where {T, S, F}

    figure = GeneTools.setup_figure()
    gl = figure[1, 1] = GridLayout()
    x = diag.coords.x
    y = diag.coords.y
    z = diag.coords.z
    spec_index = findfirst(s -> s === spec, S) 
    for (vᵢ, v) in enumerate(contours.var_list)
        ax = Axis(gl[vᵢ, 1], ylabel = L"y/\rho_\textrm{s}", xlabel = L"x/\rho_\textrm{s}", 
                  limits = ((first(x), last(x)), (first(y), last(y))), xautolimitmargin = (0., 0.),
                  yautolimitmargin = (0., 0.))
        pd = view(contours.plot_data[v], :, :, 1, spec_index)
        hilo = max(abs.(extrema(pd))...)
        clevels = collect(range(start = -hilo, stop = hilo, length = 51))
        co = contourf!(ax, x, y, pd, colormap = :balance, levels = clevels)
        cb = Colorbar(gl[vᵢ, 2], co)
    end
    return figure
end 
