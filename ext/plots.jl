function GeneTools.setup_figure(;
                                size = (1200, 800),
                                fontsize = 48, 
                                kwargs...
                               )
    textheme = Theme(fonts=(; regular=texfont(:text),
    bold=texfont(:bold),
    italic=texfont(:italic),
    bold_italic=texfont(:bolditalic)))

    fig = with_theme(textheme) do
        Figure(; size = size, fontsize = fontsize, kwargs...)
    end
    return fig
end


#Data plotting routines
function GeneTools.plot_diagnostic(diag::GeneTools.DiagControl)
    for d in diag.diagnostics
        for (_, s) in enumerate(GeneTools.species(d))
            name = GeneTools.get_diag_name(d)
            if s != :none
                fid = h5open(diag.out_path * name * "_" * String(s) * "_" * diag.run_IDs[1] * ".h5", "r")   
                GeneTools.plot_diagnostic(diag,d,s,fid)
                close(fid)

            end
        end
    end
end

function GeneTools.plot_scan_diagnostic(diag::GeneTools.DiagControl)
    for d in diag.diagnostics
        for (_, s) in enumerate(GeneTools.species(d))
            name = GeneTools.get_diag_name(d)
            fid_name = s == :none ? diag.out_path * name * "_scan.h5" : diag.out_path * name * "_scan_" * String(s) * ".h5" 
            fid = h5open(fid_name, "r")
            GeneTools.plot_scan_diagnostic(diag,d,s,fid)
            close(fid)
        end
    end
end
