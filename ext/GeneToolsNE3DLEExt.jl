
module GeneToolsNE3DLEExt

using GeneTools
using NE3DLE
using PlasmaEquilibriumToolkit
using StaticArrays
using LinearAlgebra

function Ba(surface::NE3DLESurface)
    return surface.norms.B_ref
end

function s0(surface::NE3DLESurface, args...)
    return (surface.norms.ρ/surface.norms.L_ref)^2
end

function iota(surface::NE3DLESurface)
    return surface.params.iota
end

function shat(surface::NE3DLESurface, args...)
    return surface.params.surface_quant["grad_iota"]/surface.params.iota*(-surface.norms.dpsi_drho*surface.norms.ρ)
end

function R_major(surface::NE3DLESurface)
    return surface.norms.R0
end

function A_minor(surface::NE3DLESurface)
    return surface.norms.L_ref
end

function dpdx(surface::NE3DLESurface, coords)
    return -2*surface.norms.mu_0*surface.norms.dpsi_drho*surface.norms.L_ref*surface.params.surface_quant["grad_p"]/(surface.norms.B_ref^2)
end

function (::GeneTools.GeneFromPest)(p::PET.PestCoordinates,
                          surface::NE3DLESurface;
                         )
    Φ = surface.norms.L_ref^2*surface.norms.B_ref/2       #Edge flux
    x = sqrt(p.ψ/Φ)
    y = x*p.α
    z = p.α + surface.params.iota * p.ζ
    return GeneTools.GeneCoordinates(x, y, z)
end


# Transform coordinates to the GENE coordinate system
# Note the ∇ are actually the normalized hat{∇}
function PlasmaEquilibriumToolkit.transform_basis(::GeneTools.GeneFromPest,
                                                  p::PET.PestCoordinates,
                                                  e::PET.BasisVectors,
                                                  surface::NE3DLESurface;
                                                 )
    L_ref = surface.norms.L_ref
    B_ref = surface.norms.B_ref
    dψdx = surface.norms.dpsi_drho
    ι = surface.params.iota
    ∇ι = surface.params.surface_quant["grad_iota"]
    ∇x = e[:,1]/dψdx
    ∇y = dψdx/B_ref * (e[:,2] - p.α/ι * ∇ι * e[:, 1])
    ∇z = L_ref*(e[:,2] + p.ζ*∇ι*e[:,1] + ι*e[:,3])
    return hcat(∇x, ∇y, ∇z)
end

function GeneTools.gene_geometry_coefficients(::GeneTools.GeneFromPest,
                                    p::PET.PestCoordinates,
                                    surface::NE3DLESurface;)
    flux_coords = NE3DLE.FluxFromPest()(p,surface)
    flux_contra_basis = basis_vectors(Contravariant(),CartesianFromFlux(),flux_coords,surface)
    pest_contra_basis = transform_basis(PestFromFlux(),flux_coords,flux_contra_basis,surface)
    gene_contra_basis = transform_basis(GeneFromPest(), p, pest_contra_basis,surface)
    gene_cov_basis = transform_basis(CovariantFromContravariant(), gene_contra_basis)
    ∇B = grad_B(flux_coords, flux_contra_basis, surface)
    gene_metric = PlasmaEquilibriumToolkit.metric(gene_contra_basis)
    B_norm_mag = norm(cross(gene_contra_basis[:,1], gene_contra_basis[:,2]))
    jac = abs(1.0/dot(gene_contra_basis[:,1], cross(gene_contra_basis[:,2], gene_contra_basis[:,3])))
    L1, L2 = surface.norms.L_ref/surface.norms.B_ref.*grad_B_projection(gene_contra_basis,∇B)
    dBdZ = surface.norms.L_ref/surface.norms.B_ref*dot(∇B, gene_cov_basis[:,3])
    #There is a sign inversion on the output of a GIST file
    return gene_metric, B_norm_mag, jac, -L1, L2, dBdZ
end

function GeneTools.gene_geometry_coefficients(::GeneTools.GeneFromPest,
                                    p::AbstractArray{PET.PestCoordinates{T,T}},
                                    surface::NE3DLESurface
                                   ) where {T}
    gene_metric = Array{SVector{6,Float64}}(undef, size(p))
    L1 = Array{Float64}(undef, size(p))
    L2 = similar(L1)
    dBdZ = similar(L1)
    B_norm_mag = similar(L1)
    jac = similar(L1)

    for i in eachindex(p)
        gene_metric[i], B_norm_mag[i], jac[i], L1[i], L2[i], dBdZ[i] = GeneTools.gene_geometry_coefficients(GeneFromPest(),p[i],surface)
    end
    return gene_metric, B_norm_mag, jac, L1, L2, dBdZ
end

function write_gene_geometry(surface::NE3DLESurface,
                             filename::String;
                             α₀::Union{T,Vector{T}}=-surface.params.field_line_label/surface.params.iota,
                             nz0::Int=surface.params.parallel_resolution,
                             pol_turns::Int=convert(Int,ceil(surface.params.computed_field_periods*surface.params.iota/surface.params.nfp)),
                            ) where T
    ι = surface.params.iota

    ζ_range = range(α₀-pol_turns*π/ι,step=2*π*pol_turns/nz0/ι,length=nz0)

    points = PET.MagneticCoordinateCurve(PestCoordinates,surface.norms.ρ^2*surface.norms.B_ref,-ι*α₀,ζ_range)

    metric, modB, sqrtg, K1, K2, ∂B = GeneTools.gene_geometry_coefficients(GeneFromPest(),points,surface)
    GeneTools.write_gene_geometry(filename, points, surface, metric, modB, sqrtg, K1, K2, ∂B)
end

end
