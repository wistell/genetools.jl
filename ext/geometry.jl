function GeneTools.compare_geometry(geom_file::String,
				    geom_type::String,
                                    vars_per_plot::Int,
                                    var_list::Union{Symbol, Vector{Symbol}};
				    color_list::Vector{Any} = [],
				    line_styles::Vector{Symbol} = fill(:solid,length(var_list)),
				    out_path = ".",
                                    kwargs...)
    geom = GeneTools.set_geometry(geom_file,geom_type)
    geom_dict = Dict(:gxx => geom.gxx,
                     :gxy => geom.gxy,
                     :gyy => geom.gyy,
                     :jac => geom.jac,
                     :modB => geom.modB,
                     :K2 => geom.K2,
                     :K1 => geom.K1,
                     :dBdz => geom.dBdz
                     )
    n_vars = length(var_list)
    z = geom.theta
    figure = GeneTools.setup_figure()
    legend_labels = Vector{Any}(undef,vars_per_plot)
    n_subfigures = Int(ceil(n_vars/(vars_per_plot)))
    ax = nothing
    for f in 1:n_subfigures
        gl = figure[f,1] = GridLayout()
        elem = Vector{Any}(undef,vars_per_plot)
	legend_labels = Vector{Any}(undef,vars_per_plot)
        for v in 1:vars_per_plot
	    line_color = color_list == [] ? Makie.wong_colors()[v] : color_list[v]
            var_idx = 2*(f-1) + v
            var = var_list[var_idx]
            var_label = LaTeXString("\$$(plot_geometry_labels[var])\$")
            y_ax_pos = v == 1 ? :left : :right
            if f == n_subfigures
                ax = Axis(gl[1, 1];
                          ylabel = var_label,
                          xlabel = L"z/\pi",
                          yaxisposition = y_ax_pos, 
                          kwargs...)
            else
                ax = Axis(gl[1, 1];
                          ylabel = var_label,
                          yaxisposition = y_ax_pos,
                          kwargs...)
            end
            ax.xgridvisible = false
            ax.ygridvisible = false
            if var == :gxx || var == :gyy
                ylims!(ax,0,nothing)
            end
            if var == :K2 || var == :K1 || var == :gxy || var == :dBdz
                hlines!(ax,0,color=:black,linewidth=0.75)
		#hlines!(ax,1,color=:black,alpha=0)
            end
            
            lines!(ax,z,geom_dict[var],
                   color = line_color,
                   linewidth = 3,
                   label = var_label,
		   linestyle = line_styles[v])
            legend_labels[v] = var_label
            elem[v] = LineElement(color = line_color, 
				  linestyle = line_styles[v], 
                                  linewidth = 3,
                                  points = Point2f[(0, 0.5), (1, 0.5)])
        end
        axislegend(ax,elem,legend_labels)
    end
    out_name = out_path * "geometry"

    for v in var_list
        out_name = out_name * "_" * String(v)
    end

    save(out_name * ".png",figure)
end
