
module GeneToolsVMECExt

using GeneTools
using VMEC
using PlasmaEquilibriumToolkit
using StaticArrays
using LinearAlgebra

function GeneTools.Ba(eq::VMEC.VmecSurface)
    return abs(eq.phi[end]/(π*eq.Aminor_p^2))
end

function GeneTools.s0(eq::VMEC.VmecSurface, coords)
    return getfield(coords[1], 1)*2π/eq.phi[end]*eq.signgs
end

function GeneTools.iota(eq::VMEC.VmecSurface)
    return eq.iota[1]
end   

function GeneTools.shat(eq::VMEC.VmecSurface ,coords)
    q0 = 1/eq.iota[1]
    return -2*GeneTools.s0(eq, coords)/q0*eq.iota[2]*q0^2
end

function GeneTools.R_major(eq::VMEC.VmecSurface)
    return eq.Rmajor_p
end

function GeneTools.A_minor(eq::VMEC.VmecSurface)
    return eq.Aminor_p
end

function GeneTools.dpdx(eq::VMEC.VmecSurface, coords)
    return -4.0*sqrt(GeneTools.s0(eq,coords))*eq.pres[2]/(GeneTools.Ba(eq)^2)*4π*1e-7
end

"""
    (::GeneFromPest)(p::PestCoordinates, vmec_surface::VmecSurface)

Transform coordinates to the GENE coordinate system
# x = ``sqrt(ψ*2π/ψ_lcfs)``
# ∇x = ∇ψ dx/dψ = ∇ψ 1/(2 x) 2π / ψ_lcfs
# y = x * αₚ
# ∇y = ∇(αₚ/ι)|ι₀|x₀
#    = x(∇αₚ + αₚ * 2π/ψ_lcfs * ι'/ι * ∇ψ)
# z = αₚ - ι * ζ
# ∇z = ∇αₚ - ∇ι * ζ - ι * ∇ζ
#    = ∇αₚ - 2π/ψ_lcfs * ι' * ∇ψ * ζ - ι * ∇ζ
"""
function (::GeneTools.GeneFromPest)(p::PET.PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  Φ = vmecsurf.phi[end]*vmecsurf.signgs
  x = sqrt(p.ψ*2π/Φ)
  y = x*p.α
  z = p.α - vmecsurf.iota[1] * p.ζ
  return GeneTools.GeneCoordinates(x, y, z)
end


function PlasmaEquilibriumToolkit.transform_basis(::GeneTools.GeneFromPest,
                                                  p::PET.PestCoordinates,
                                                  e::PET.BasisVectors,
                                                  vmecsurf::VMEC.VmecSurface;
                                                 )
    Aminor = vmecsurf.Aminor_p
    Φ = vmecsurf.phi[end]*vmecsurf.signgs
    x = sqrt(p.ψ*2π/Φ)
    ∇x = Aminor*π/(Φ*x)*e[:,1]
    # Multiple by sign(Φ) to ensure coordinate system is left handed
    ∇y = sign(Φ) * Aminor * x * (e[:,2] + p.α/vmecsurf.iota[1] * vmecsurf.iota[2] * 2π/Φ * e[:, 1])
    ∇z = Aminor*(e[:,2] - p.ζ*vmecsurf.iota[2]*2π/Φ*e[:,1] - vmecsurf.iota[1]*e[:,3])
    return hcat(∇x, ∇y, ∇z)
end

"""
    gene_geometry_coefficients(::GeneFromPest, p::PestCoordinates, vmec_surf::VmecSurface)
    gene_geometry_coefficients(::GeneFromPest, p::StructArray{PestCoordinates}, vmec_surf::VmecSurface)

Compute the normalization geometric quantities for a GENE simulation
from a point or set of points specfied in `PestCoordinates` on a `VMEC`surface.
"""
function GeneTools.gene_geometry_coefficients(::GeneTools.GeneFromPest,
                                    p::PET.PestCoordinates,
                                    vmecsurf::VMEC.VmecSurface;
                                   )
    Aminor = vmecsurf.Aminor_p
    Ba = abs(vmecsurf.phi[end])/(π*Aminor^2)
    v = VmecFromPest()(p,vmecsurf)
    vmecContraBasis = basis_vectors(Contravariant(), CartesianFromVmec(), v, vmecsurf)
    pestContraBasis = transform_basis(PestFromVmec(), v, vmecContraBasis,vmecsurf)
    geneContraBasis = transform_basis(GeneFromPest(), p, pestContraBasis,vmecsurf)
    geneCoBasis = transform_basis(CovariantFromContravariant(), geneContraBasis)
    gB = grad_B(v, vmecContraBasis, vmecsurf)
    geneMetric = metric(geneContraBasis)
    BnormMag = norm(cross(geneContraBasis[:,1], geneContraBasis[:,2]))
    # The Jacobian should always be negative, but as only the integral measure
    # is required, that must be positive negative
    jac = abs(1.0/dot(geneContraBasis[:,1], cross(geneContraBasis[:,2], geneContraBasis[:,3])))
    K1, K2 = Aminor/Ba.*grad_B_projection(geneContraBasis,gB)
    dBdZ = Aminor/Ba*dot(gB, geneCoBasis[:,3])
    # The GENE K1 component is the negative of what is computed, see the GIST documentation note
    return geneMetric, BnormMag, abs(jac), -K1, K2, dBdZ
end

function GeneTools.gene_geometry_coefficients(::GeneTools.GeneFromPest,
                                    p::AbstractArray{PET.PestCoordinates{T,T}},
                                    vmecsurf::VMEC.VmecSurface
                                   ) where {T}
  gene_metric = Array{SVector{6,Float64}}(undef, size(p))
  K1 = Array{Float64}(undef, size(p))
  K2 = similar(K1)
  dBdZ = similar(K1)
  B_norm_mag = similar(K1)
  jac = similar(K1)
  for i in eachindex(p)
    gene_metric[i], B_norm_mag[i], jac[i], K1[i], K2[i], dBdZ[i] = GeneTools.gene_geometry_coefficients(GeneTools.GeneFromPest(),p[i],vmecsurf)
  end
  return gene_metric, B_norm_mag, jac, K1, K2, dBdZ
end

function GeneTools.write_gene_geometry(vmec::VMEC.Vmec,
                             s0::T,
                             α₀::Union{T,Vector{T}},
                             nz0::Int,
                             pol_turns::Int,
                             filename::String;
                            ) where T
    vmec_surf = VMEC.VmecSurface(s0,vmec)

    # VMEC ι is left-handed, PEST ι is right handed
    ι = -vmec_surf.iota[1]

    ζ_range = range(α₀-pol_turns*π/ι,step=2*π*pol_turns/nz0/ι,length=nz0)

    points = PET.MagneticCoordinateCurve(PET.PestCoordinates,vmec_surf.phi[end]*s0*vmec_surf.signgs/(2π),-ι*α₀,ζ_range)

    metric, modB, sqrtg, K1, K2, ∂B = GeneTools.gene_geometry_coefficients(GeneTools.GeneFromPest(),points,vmec_surf)
    GeneTools.write_gene_geometry(filename, points, vmec_surf, metric, modB, sqrtg, K1, K2, ∂B)
end

"""
This function uses a vmec equilibrium file and gene info to map gene coordinates 
to real space coordinates and outputs a matrix of those points.
Example:
using GeneTools, VMEC, NetCDF

wout = NetCDF.open("/path/to/your/wout/file.nc");
vmec= VMEC.readVmecWout(wout);
diag = GeneTools.Diag()
diag = GeneTools.setDiagModDir!(diag)
diag = GeneTools.startGeneDiag()
GeneTools.readDiagFile!(diag,"/home/gwheld/research/gradResearch/diag.in")
runID = "5" #whatever the run number of your file is
parDict = diag.parDicts[runID]
s = 0.16
α0 = 3.14
grids = makeRealSpaceFluxTubeGrid(s,α0,parDict,vmec)
"""
function GeneTools.make_real_space_flux_tube_grid(s::F,
                                        α0::F,
                                        parameters::D,
                                        vmec::Vmec;
                                        ρ::Float64=0.001
                                       ) where {F, D <: AbstractDict}
    T = parameters[:info][:PRECISION]
    nz = parameters[:box][:nz0]
    nx = parameters[:box][:nx0]
    ny = parameters[:box][:nky0]
    npol = parameters[:geometry][:n_pol]
    ψ = s * vmec.phi(1.0) / (2π) * vmec.signgs
    ζ = LinRange(-π*npol/vmec.nfp,π*npol/vmec.nfp,nz)
    pest_coords = MagneticCoordinateCurve(PestCoordinates, ψ, α0, ζ)
    vmec_s = VMEC.VmecSurface(s, vmec)
    e_pest = basis_vectors(Contravariant(), CartesianFromPest(), pest_coords, vmec_s)
    e_contra_gene = transform_basis(GeneTools.GeneFromPest(), pest_coords, e_pest, vmec_s)  # This will be contravariant
    basis_vectors = Vector{Matrix{T}}(undef, nz)
    for i in eachindex(e_contra_gene)
        basis_matrix = Matrix{T}(undef,3,3)
        basis_matrix[1:3] = e_contra_gene[i][1:3]./norm(e_contra_gene[i][1:3],2)
        basis_matrix[4:6] = e_contra_gene[i][4:6]./norm(e_contra_gene[i][4:6],2)
        basis_matrix[7:9] = e_contra_gene[i][7:9]./norm(e_contra_gene[i][7:9],2)
        basis_vectors[i] = basis_matrix
    end
    #v = VmecFromPest()(pest_coords,vmec_s)
    #cartCoords = CartesianFromVmec()(v, vmec_s);
    cartCoords = CartesianFromPest()(pest_coords, vmec_s)
    X = [q[1] for q in cartCoords];
    Y = [q[2] for q in cartCoords];
    Z = [q[3] for q in cartCoords];
    #grids = Array{Vector}(undef,nz,nx,ny)
    #vals = Array{ComplexF64}(undef,nz,nx,ny)
    gridpoints = Array{SVector{3, T}}(undef, nz, nx, ny)
    vals = zeros(Complex{T}, nz, nx, ny)
    dx=parameters[:box][:lx] * ρ / (nx - 1)
    dy=parameters[:box][:ly] * ρ / (ny - 1)
    for i in 1:nz
        for j in 1:nx
            for k in 1:ny
                x = X[i] + (j - nx / 2)* dx * basis_vectors[i][1]+(k - ny / 2) * dy * basis_vectors[i][4]
                y = Y[i] + (j- nx / 2)* dx * basis_vectors[i][2] + (k - ny / 2) * dy * basis_vectors[i][5]
                z = Z[i] + (j - nx / 2) * dx * basis_vectors[i][3] + (k - ny / 2) * dy * basis_vectors[i][6]
                gridpoints[i,j,k] = [x,y,z]
                #vals[i,j,k] = 1.0 + 1.0im
            end 
        end
    end
    return GeneTools.FluxTube{T}(gridpoints, vals, s, α0, ρ)
end


end
