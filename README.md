# GENETools
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://bfaber.gitlab.io/GeneTools.jl/dev)
[![Build Status](https://gitlab.com/bfaber/GeneTools.jl/badges/main/pipeline.svg)](https://gitlab.com/bfaber/GeneTools.jl/pipelines)
[![Coverage](https://gitlab.com/bfaber/GeneTools.jl/badges/main/coverage.svg)](https://gitlab.com/bfaber/GeneTools.jl/commits/main)

A tool for post-processing data from the [Gyrokinetic Electromagnetic Numerical Experiment](genecode.org) plasma turbulence simulation code.

Authors:
Benjamin Faber (bfaber (AT) wisc.edu)
Jason Smoniewski (smoniewski (AT) wisc.edu)
Joey Duff (jduff2 (AT) wisc.edu)
