
start_time = 278.0
end_time = 280.0
last_time_step = true
data_path = "/pscratch/sd/b/bfaber/HSX_He_modeling/nonlinear"
run_IDs = "161"
plot_type = :CairoMakie
active_species = [:i, :e]
diagnostics = [:(Contours((:phi, )))]
sparse_step = 1
time_average = false
