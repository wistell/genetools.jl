module GeneTools

using Measures
using FortranFiles
using LinearAlgebra
using StaticArrays
using StatsBase
using ComponentArrays
using DataStructures
using FastGaussQuadrature
using FFTW
using CoordinateTransformations
using Printf
using PlasmaEquilibriumToolkit
using Interpolations
using HDF5

include("base/GeneDict.jl")
include("base/diagnostic_definitions.jl")
include("base/types.jl")
include("base/utils.jl")


# Include all the base code
include("base/timeslice_data.jl")
include("base/fileOps.jl")
include("base/fileIO.jl")
include("base/diag.jl")
include("base/grid.jl")
include("base/geometry.jl")


# Include all the diagnostic code
include("include/flux_spectra.jl")
include("include/contours.jl")
include("include/field.jl")
include("include/nrg.jl")
include("include/cross_phases.jl")
include("include/eigenvalues.jl")
include("include/ballooning.jl")
include("include/z_profile.jl")
include("include/visualization.jl")

export AbstractGeneDiagnostic, get_fields_and_moments, add_plot_data
export NRG, FluxSpectra

export DiagControl, GeneMomentID, GeneTrapPassMomentID, GeneFieldID, GeneIDVector
export GeneData, OmegaData, EigenvalueData, GeneFileInfo, GeneGrid, GeneGeom 

export GeneTimesliceData, get_gene_data, get_gene_data!, get_file_info, read_parameters_file, get_isteps
export start_gene_diag, reset_diag!, read_diag_file, get_data_times, get_sim_times, run_diags!, run_scan_diags!

export set_grid_coords!, make_real_space_flux_tube_grid
export set_geometry!, read_gist_file!, write_gene_geometry

end
