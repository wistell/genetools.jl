"""
    getMomData
New function for getting momentum data which allows for the approximate specification of start and stop times
as well as a sparase factor. This allows for a limitation of data (in the case where accessing all the data
is not possible due to memory issues)
"""
function getMomData(diag::Diag,runID::String,spec::String,start::Float64,stop::Float64,sparseFactor::Int64)
  if start > stop
    println("ERROR: Start time must be less than or equal to stop time.")
    return
  end
  momData = GeneData()
  key = "mom_"*spec*"_"*runID
  momInfo = diag.dataFiles[key]
  # find which mom records correspond to specified times
  sim_first_index = findmin(abs.(diag.simTimes .- start),dims =1)[2][1]
  sim_last_index = findmin(abs.(diag.simTimes .- stop),dims =1)[2][1]
  mom_first_index = sim_first_index/length(diag.simTimes)*momInfo.records > 1 ? trunc(Int64,sim_first_index*momInfo.records/length(diag.simTimes))  :  1 
  mom_last_index = sim_last_index/length(diag.simTimes)*momInfo.records > 1 ? trunc(Int64,sim_last_index*momInfo.records/length(diag.simTimes))  :  1 
  numTimes = trunc(Int64,(mom_last_index-mom_first_index)/sparseFactor) > 1 ? trunc(Int64,(mom_last_index-mom_first_index)/sparseFactor)  :  1
  momTimeLocs = Vector{Int64}(undef,numTimes)
  # define a vector that contains both the times and which record is being used from the momentum file
  for i in 0:numTimes-1
    momTimeLocs[i+1]=mom_first_index+i*sparseFactor
  end
  T = momInfo.dataType
  blockSize = momInfo.blockDims[end]
  times = Array{Float64}(undef,numTimes);
  dataArr = Array{T}(undef,Tuple(vcat(momInfo.blockDims,numTimes)))
  momLocs = map(i->i,range(1,stop=blockSize))
  times = readGeneBinary!(momInfo,dataArr,momTimeLocs,momLocs)
  momData.times = times
  momData.data = dataArr
  return momData
end