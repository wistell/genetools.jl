"""
This diagnostic is to find the quasilinear heat flux from a linear GENE scan.
There are several restrictions to the type of linear GENE scans:
nx0 = 1
scan over ky, kx_center
Electrostatic
"""
mutable struct QLHFData
  C::Float64
  chi_i::Float64
  chi_e::Float64
  Q_i::Float64
  Q_e::Float64
  Q_tot::Float64
end

function QLHFSetup()
  return fileList = ["qlhf"]
end

#Only good for ions/one-species
#=
Takes in linear scan data and outputs the kx and ky vectors and the kx- and ky-averaged quasilinear heat flux
=#
function QLHFSpectrum(diag::Diag;Sk::Array{Float64}=[1.0],ky_vec::Array{Float64}=[0.0],C::Float64=1.0,omega_ZF::Float64=0.0)
  num_runs = length(diag.runIDs)
  runID1 = diag.runIDs[1]
  n_ev = diag.parDicts[runID1]["comp_type"][1] == "IV" ? 1 : haskey(diag.parDicts[runID1],"n_ev") ? diag.parDicts[runID1]["n_ev"][1] : 1
  kx_scan,ky_scan,gamma,omega = getLinearScanEigenvalues(diag)
  diag = extendFluxTube!(diag)
  gxx = diag.geom.gxx
  gxy = diag.geom.gxy
  gyy = diag.geom.gyy
  jac = diag.geom.jac
  omti = diag.parDicts[runID1]["omt"][1]
  Q = zeros(Float64,(length(ky_scan),length(kx_scan)))
    for i in 1:num_runs
      runID = diag.runIDs[i]

      ky = diag.parDicts[runID]["kymin"][1]
      if ky_vec == [0.0]
        ky_match_idx = 1
      else
        ky_match_idx = findall(x->x==ky,ky_vec)
        if length(ky_match_idx) == 0 || maximum(ky_vec)<ky 
          ky_match_idx = length(ky_vec)
        elseif length(ky_match_idx) == 1 
          ky_match_idx = ky_match_idx[1]
        end
      end
        
      # Assumes nx0 = 1. Maybe should relax restriction...
      if haskey(diag.parDicts[runID],"kx_center")
        kx = diag.parDicts[runID]["kx_center"][1]
      else
        kx = 0#2*pi/diag.parDicts[runID]["lx"][1]
      end

      scan_idx = (findall(x->x==ky,ky_scan)[1],findall(x->x==kx,kx_scan)[1])

      fieldData = getFieldData(diag,runID)
      fieldData.data = extendFieldData(diag,runID)
      
      nrgData = getNrgData(diag,runID)

      field_time_steps = length(fieldData.times)

      for j in 1:n_ev
        gamma_kj = isnan(gamma[j][scan_idx[1],scan_idx[2]]) || gamma[j][scan_idx[1],scan_idx[2]] < 0.0 ? 0.0 : gamma[j][scan_idx[1],scan_idx[2]]
	if gamma_kj > omega_ZF
          nrg_time_steps = diag.parDicts[runID]["comp_type"][1] == "IV" ? length(nrgData.times) : j
          field_time_steps = diag.parDicts[runID]["comp_type"][1] == "IV" ? length(fieldData.times) : j
          eig_func = fieldData.data[1,:,1,field_time_steps]
          ef2 = real.(eig_func .* conj.(eig_func))
          kp_numerator = sum(jac.*ef2.*(kx^2*gxx.+kx*ky*gxy.+ky^2*gyy))
          kp_denominator = sum(jac.*ef2)
          k_perp_ave = kp_numerator/kp_denominator
	
	  Qes = nrgData.data[7,1,nrg_time_steps]
          n2 = nrgData.data[1,1,nrg_time_steps]
          weight = Qes/n2

          Q[scan_idx[1],scan_idx[2]] += real((gamma_kj-omega_ZF)/k_perp_ave)*weight*Sk[ky_match_idx]
        end
      end
    end
  Q .*= omti*C
  kxSpec = sum(Q,dims=1)[1,:]#./length(kx_scan)
  kySpec = sum(Q,dims=2)[:,1]#./length(ky_scan)
  setGeometry!(diag)
  return kx_scan,ky_scan,kxSpec,kySpec,Q
end

function plotQLHFSpectrum(diag::Diag,xORy::String;Sk::Array{Float64}=[1.0],ky_vec::Array{Float64}=[0.0],C::Float64=1.0,omega_ZF::Float64=0.0)
  kx,ky,kxSpec,kySpec = QLHFSpectrum(diag,Sk=Sk,ky_vec=ky_vec,C=C,omega_ZF=omega_ZF)
  fig,ax = PyplotInit()
  ylabel("\$Q_{es} \\quad (c_sn_{i0}T_{i0}(\\rho_s/a)^2)\$")
  if xORy == "x"
    xlabel("\$k_x\\rho_s\$")
    ax.plot(kx,kxSpec,"o",markerfacecolor="none",ls="-",label="\$Q_{es}^{QL}\$")
  elseif xORy == "y"
    xlabel("\$k_y\\rho_s\$")
    ax.plot(ky,kySpec,"o",markerfacecolor="none",ls="-",label="\$Q_{es}^{QL}\$")
  end
  ax.set_ylim(bottom=0.0)
  tight_layout()
    
  return fig,ax
end

function shapeFactor(diag::Diag,NLFlux::Array{Float64},ky_NLF::Array{Float64})
  kx,ky,kxSpec,kySpec,Q = QLHFSpectrum(diag)
  if ky[1]%ky_NLF[2] == 0 && ky[1]>=ky_NLF[2]
    NLFlux = NLFlux[2:Int(ky[1]/ky_NLF[2]):end]
  end
  if length(kySpec)<length(NLFlux)
    NLFlux = NLFlux[1:length(kySpec)]
  end
  Sk = NLFlux./kySpec
  return Sk
end

function QuaLiKiz(diag::Diag;C::Float64=1.0,omega_ZF::Float64=0.0)
  kx,ky,kxSpec,kySpec,Q = QLHFSpectrum(diag,omega_ZF=omega_ZF)
  maxgkp2 = maximum(kySpec)
  maxky = findall(x->x==maximum(kySpec),kySpec)[1][1]*minimum(ky)
  kyQLK = zeros(length(ky))
  for i in 1:length(ky)
    if ky[i]<=0.3
      kyQLK[i] = kySpec[i]*ky[i]/(maxky^2)*C*maxgkp2
    else
      kyQLK[i] = kySpec[i]*maxky^2/(ky[i]^3)*C*maxgkp2
    end
  end
  return kyQLK
end

function diagQLHF(diag::Diag;Sk::Array{Float64}=[1.0],ky_vec::Array{Float64}=[0.0],C::Float64=1.0)
  quasiLin = QLHFData(C,0.0,0.0,0.0,0.0,0.0)
  num_runs = length(diag.runIDs)
  runID1 = diag.runIDs[1]
  diag = extendFluxTube!(diag)
  gxx = diag.geom.gxx
  gxy = diag.geom.gxy
  gyy = diag.geom.gyy
  jac = diag.geom.jac
  n_spec = diag.parDicts[runID1]["n_spec"][1]
  Q = Array{Float64}(undef,1,n_spec)
  chi = zeros(Float64,(1,n_spec))

  for spec in 1:n_spec
    #Set up n, grad_T TODO: can you find n from omn?
    n = 1 #diag.parDicts[runID1]["omn"]
    grad_T = diag.parDicts[runID1]["omt"][1]

    for i in 1:num_runs
      runID = diag.runIDs[i]

      ky = diag.parDicts[runID]["kymin"][1]
      if ky_vec == [0.0]
        ky_match_idx = 1
      else
        ky_match_idx = findall(x->x==ky,ky_vec)
        if length(ky_match_idx) == 0 || maximum(ky_vec)<ky 
          ky_match_idx = length(ky_vec)
        elseif length(ky_match_idx) == 1 
          ky_match_idx = ky_match_idx[1]
        end
      end
        
      # Assumes nx0 = 1. Maybe should relax restriction...
      if haskey(diag.parDicts[runID],"kx_center")
        kx = diag.parDicts[runID]["kx_center"][1]
      else
        kx = 0#2*pi/diag.parDicts[runID]["lx"][1]
      end
      fieldData = getFieldData(diag,runID)
      fieldData.data = extendFieldData(diag,runID)
      omegaData = diag.parDicts[runID]["comp_type"][1] == "IV" ? getOmegaData(diag,runID) : getEigenvalueData(diag,runID)
      nrgData = getNrgData(diag,runID)

      n_ev = length(omegaData.gamma)

      field_time_steps = length(fieldData.times)

      for j in 1:n_ev
        nrg_time_steps = diag.parDicts[runID]["comp_type"][1] == "IV" ? length(nrgData.times) : j
        field_time_steps = diag.parDicts[runID]["comp_type"][1] == "IV" ? length(fieldData.times) : j
        eig_func = fieldData.data[1,:,spec,field_time_steps]
        ef2 = real.(eig_func .* conj.(eig_func))
        kp_numerator = sum(jac.*ef2.*(kx^2*gxx.+kx*ky*gxy.+ky^2*gyy))
        kp_denominator = sum(jac.*ef2)
        k_perp_ave = kp_numerator/kp_denominator
	
	Qes = nrgData.data[7,spec,nrg_time_steps]
        n2 = nrgData.data[1,spec,nrg_time_steps]
        weight = Qes/n2
        gamma = omegaData.gamma[j] > 0.0 ? omegaData.gamma[j] : 0.0
        chi[spec] += real(gamma/k_perp_ave)*weight*Sk[ky_match_idx]
      end
    end
    Q[spec] = quasiLin.C*n*chi[spec]*grad_T
  end

  quasiLin.chi_i = chi[1]
  quasiLin.Q_i = Q[1]

  if n_spec == 2
    quasiLin.chi_e = chi[2]
    quasiLin.Q_e = Q[2]
  end

  quasiLin.Q_tot = quasiLin.Q_i + quasiLin.Q_e

  setGeometry!(diag)

  return quasiLin
end
