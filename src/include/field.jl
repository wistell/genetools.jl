#=
# Collection of routines for dealing with field files

function fieldSetup()
  return fileList = [:field]
end

function getScanKxKy(diag::DiagControl)
    run_IDs = diag.run_IDs
    num_runs = length(run_IDs)
    ky1 = diag.par_dicts[run_IDs[1]][:box][:kymin]
    ky2 = diag.par_dicts[run_IDs[2]][:box][:kymin]
    kx1 = haskey(diag.par_dicts[run_IDs[1]][:box], :kx_center) ? diag.par_dicts[run_IDs[1]][:box][:kx_center] : 0.0
    kx2 = haskey(diag.par_dicts[run_IDs[2]][:box], :kx_center) ? diag.par_dicts[run_IDs[2]][:box][:kx_center] : 0.0
    kyend = diag.par_dicts[run_IDs[num_runs]][:box][:kymin]
    kxend = haskey(diag.par_dicts[run_IDs[num_runs]][:box], :kx_center) ? diag.par_dicts[run_IDs[num_runs]][:box][:kx_center] : 0.0

    kx = []
    ky = []

    if ky1 != ky2
        dky = ky2-ky1
        nky = round(Int,(kyend-ky1)/dky) + 1
        dkx = haskey(diag.par_dicts[run_IDs[nky+1]][:box], :kx_center) ? diag.par_dicts[run_IDs[nky+1]][:box][:kx_center]-kx1 : 0.0-kx1
        nkx = dkx==0 ? 0 : Int((kxend-kx1)/dkx + 1)
        for i in 0:nkx-1
            append!(ky,[ky1:dky:kyend;])
            append!(kx,fill(kx1+dkx*i,nky))
        end
    else
        dkx = kx2-kx1
        nkx = Int((kxend-kx1)/dkx + 1)
        dky = diag.par_dicts[run_IDs[nkx+1]][:box][:kymin]-ky1
        nky = Int((kyend-ky1)/dky + 1)
        for i in 1:nky-1
            append!(kx,[kx1:dkx:kxend;])
            append!(ky,fill(ky1+dky*i,nkx))
        end
    end

    return kx, ky
end


"""
    get_field_ata
Function for getting field data at approximate start and stop times. Also implements a sparse factor
"""
function getFieldData(diag::DiagControl,
                      runID::S,
                      start::T,
                      stop::T,
                      sparseFactor::Int;
                     ) where {T, S <: AbstractString}
    if start > stop
        println("ERROR: Start time must be less than or equal to stop time.")
        return
    end
    key = "field_"*runID
    fieldInfo = diag.data_files[key]
    data_type = fieldInfo.data_type
    fieldData = GeneData{data_type}()
    sim_first_index = findmin(abs.(diag.sim_times .- start),dims =1)[2][1]
    sim_last_index = findmin(abs.(diag.sim_times .- stop),dims =1)[2][1]
    field_first_index = sim_first_index/length(diag.sim_times)*fieldInfo.records > 1 ? trunc(Int64,sim_first_index*fieldInfo.records/length(diag.sim_times))  :  1 
    field_last_index = sim_last_index/length(diag.sim_times)*fieldInfo.records > 1 ? trunc(Int64,sim_last_index*fieldInfo.records/length(diag.sim_times))  :  1 
    numTimes = trunc(Int64,(field_last_index-field_first_index)/sparseFactor) > 1 ? trunc(Int64,(field_last_index-field_first_index)/sparseFactor)  :  1
    fieldTimeLocs = Vector{Int64}(undef,numTimes)
    # define a vector that contains both the times and which record is being used from the field file
    for i in 0:numTimes-1
        fieldTimeLocs[i + 1]= field_first_index + i * sparseFactor
    end
    blockSize = fieldInfo.block_dims[end]
    times = Array{T}(undef, numTimes);
    dataArr = Array{data_type}(undef, Tuple(vcat(fieldInfo.blockDims, numTimes)))
    fieldLocs = collect(1:blockSize)
    times = read_gene_binary!(dataArr, fieldInfo, fieldTimeLocs, fieldLocs)
    fieldData.times = times
    fieldData.data = dataArr
    return fieldData
end

function getFieldData(diag::DiagControl,run_IDs::Array{String})
    fieldData = getFieldData(diag, run_IDs[1])
        for i in 2:length(run_IDs)
            fieldData.data = cat(fieldData.data, getFieldData(diag,run_IDs[i]).data, dims=5)
            fieldData.times = vcat(fieldData.times, getFieldData(diag,run_IDs[i]).times)
        end
    return fieldData
end

function getZProfile(diag::DiagControl,runID::String,kx::Int,ky::Int,timeStart::Float64,timeEnd::Float64;Phis::Array{Int}=[1,2,3])
    fieldData = getFieldData(diag,runID)
    nkx = size(fieldData.data,1)
    nky = size(fieldData.data,2)
    prof_data = Dict()

    for i in 1:length(fieldData.times)
        time = fieldData.times[i]
        fd = fieldData.data[:,:,:,:,i]
        if time >= timeStart && time <= timeEnd
            prof_data[time] = fd
        end
    end
    ntimes = length(keys(prof_data))

    z = diag.coords.z ./ pi
    nz = length(z)
    data = Array{eltype(fieldData.data), 4}(undef,nkx,nky,nz,diag.par_dicts[runID][:n_fields])

    for key in keys(prof_data)
        data .+= prof_data[key]./ntimes
    end

    data = data[kx,ky,:,1]
    nPhis = length(Phis)
    ZProfile = hcat(hcat(real.(data),imag.(data)),real.((data.*conj.(data)).^(0.5)))
    return ZProfile
end

function plotZProfile(diag::DiagControl,runID::String,kx::Int,ky::Int,timeStart::Float64,timeEnd::Float64;Phis::Array{Int}=[1,2,3])
    z = diag.coords.z ./ pi
    PhiTitles = ["\$Re(\\Phi)\$","\$Im(\\Phi)\$","\$|\\Phi|\$"]
    plotData = getZProfile(diag,runID,kx,ky,timeStart,timeEnd,Phis=Phis)
    plt = linePlot(z,plotData[:,Phis[1]],"\$z \\quad(\\pi)\$","",:right,-Inf,Inf,-Inf,Inf,PhiTitles[Phis[1]],true)
    nPhis = length(Phis)
    if nPhis>1
        for i in 2:nPhis
            plt = linePlot!(plt,z,plotData[:,Phis[i]],"\$z \\quad(\\pi)\$","",:right,-Inf,Inf,-Inf,Inf,PhiTitles[Phis[i]],true)
        end
    end

    return plt
end

function fieldTimeAverage(diag::DiagControl,run_IDs::Array{String},timeStart::Float64,timeEnd::Float64)
    fieldData = getFieldData(diag,run_IDs)
    nkx = size(fieldData.data,1)
    nky = size(fieldData.data,2)
    tsIdx = length(fieldData.times) - length(fieldData.times[fieldData.times .>= timeStart])
    teIdx = length(fieldData.times) - length(fieldData.times[fieldData.times .>= timeEnd])
    times = fieldData.times[tsIdx:teIdx]
    fd_tavg = sum(abs.(fieldData.data[:,:,:,:,tsIdx:teIdx]),dims=5)./length(times)
    return fd_tavg
end

function nonzonalEnergy(diag::DiagControl,run_IDs::Array{String},timeStart::Float64,timeEnd::Float64)
    fd_tavg = fieldTimeAverage(diag,run_IDs,timeStart,timeEnd)
    nz = size(fd_tavg,3)
    nkx = size(fd_tavg,1)
    nky = size(fd_tavg,2)
    kx_modes = 2*pi/diag.par_dicts[run_IDs[1]][:lx]*vcat([0:nkx/2;],[-nkx/2+1:-1;])
    ky_modes = 2*pi/diag.par_dicts[run_IDs[1]][:ly]*[0:nky-1;]
    kx2_mat = kron(kx_modes,transpose(ones(nky-1))).^2
    ky2_mat = kron(ones(nkx),transpose(ky_modes[2:nky])).^2
    kxky_mat = kron(kx_modes,transpose(ky_modes[2:nky]))
    fd_jac = zeros(Float64,(nkx,nky-1))
    fd_kp2 = zeros(Float64,(nkx,nky-1))
    for z in 1:nz
      fd_kp2 .+= abs.(fd_tavg[:,2:nky,z,1,1]).^2 .*diag.geom.jac[z] .* (diag.geom.gxx[z].*kx2_mat .+ diag.geom.gyy[z].*kxky_mat .+ diag.geom.gyy[z].*ky2_mat)
      fd_jac .+= abs.(fd_tavg[:,2:nky,z,1,1]).^2 .*diag.geom.jac[z]
    end
    if iseven(nkx)
    	fd_jac[div(nkx,2)+1,:] = ones(nky-1)
    end
    fd_ztavg_norm = fd_kp2./fd_jac

    nonZonalEnergy = sum(sum(fd_ztavg_norm,dims=1))/(nkx*(nky-1))
    return nonZonalEnergy
end

function plotModeTimeAverage(diag::DiagControl,run_IDs::Array{String},timeStart::Float64,timeEnd::Float64)
    fieldData = getFieldData(diag,run_IDs[1])
    for i in 2:length(run_IDs)
      fieldData.data = cat(fieldData.data,getFieldData(diag,run_IDs[i]).data,dims=5)
      fieldData.times = vcat(fieldData.times,getFieldData(diag,run_IDs[i]).times)
    end
    nkx = size(fieldData.data,1)
    nky = size(fieldData.data,2)
    kx_modes = round.(2*pi/diag.par_dicts[run_IDs[1]][:box][:lx]*vcat([0:nkx;],[-nkx:-1;]),digits=3)
    ky_modes = round.(2*pi/diag.par_dicts[run_IDs[1]][:box][:ly]*[0:nky-1;],digits=3)
    tsIdx = length(fieldData.times) - length(fieldData.times[fieldData.times .>= timeStart])
    teIdx = length(fieldData.times) - length(fieldData.times[fieldData.times .>= timeEnd])
    times = fieldData.times[tsIdx:teIdx]
    nz = size(fieldData.data,3)
    fd_jac = zeros(Float64,(nkx,nky,1,length(times)))
    for z in 1:nz
      fd_jac .+= abs.(fieldData.data[:,:,z,:,tsIdx:teIdx]).*diag.geom.jac[z]
    end
    fd_zavg = fd_jac./sum(diag.geom.jac)
    fd_ztavg = sum(fd_zavg,dims=4)./length(times)
    zonal_data = fd_ztavg[2:nkx,1,1,1]
    nonZonal_data = fd_ztavg[:,2:nky,1,1]
    maxZonal = sort(zonal_data,rev=true)[1]
    maxZMode = kx_modes[sortperm(zonal_data,rev=true)[1]]
    avgZonal = sum(zonal_data)/((nkx-1)/2)
    maxNonZonal = sort(sort(nonZonal_data,rev=true,dims=2),rev=true,dims=1)[1]
    maxNZMode = (kx_modes[Tuple.(findall(x->x==maximum(nonZonal_data),nonZonal_data))[1][1]],ky_modes[Tuple.(findall(x->x==maximum(nonZonal_data),nonZonal_data))[1][2]+1])
    avgNZonal = sum(sum(nonZonal_data,dims=1)/nkx)/(nky-1)
    return maxZonal, maxZMode, avgZonal, maxNonZonal, maxNZMode, avgNZonal
end


#Only good for one-species right now
function modeTimeTrace(diag::DiagControl,run_IDs::Array{String},timeStart::Float64,timeEnd::Float64,modes::Array{Tuple{Int,Int}};logData::Bool=false,dataMin::Float64=-Inf,dataMax::Float64=Inf)
  fieldData = getFieldData(diag.run_IDs)

    nkx = size(fieldData.data,1)
    nky = size(fieldData.data,2)
    kx_modes = round.(2*pi/diag.par_dicts[run_IDs[1]][:box][:lx]*[0:nkx-1;],digits=3)
    ky_modes = round.(2*pi/diag.par_dicts[run_IDs[1]][:box][:ly]*[0:nky-1;],digits=3)
    tsIdx = length(fieldData.times) - length(fieldData.times[fieldData.times .>= timeStart])
    teIdx = length(fieldData.times) - length(fieldData.times[fieldData.times .>= timeEnd])
    times = fieldData.times[tsIdx:teIdx]
    nz = size(fieldData.data,3)
    lineSty = fill(:solid,length(modes))
    lineCol = [1:8;]
    #=
    lineSty = Array{Symbol}(undef,length(modes))
    lineCol = zeros(Int,length(modes))
    prev_z = 0
    prev_nz = 0
    prev_st = 0
    for i in 1:length(modes)
        if modes[i][2]==1
        lineSty[i] = :solid
        lineCol[i] = prev_z == 0 ? 1 : lineCol[prev_z]+1
        prev_z = i
        elseif modes[i][1]==1
        lineSty[i] = :dot
        lineCol[i] = prev_st == 0 ? 1 : lineCol[prev_st]+1
        prev_st = i
        else
        lineSty[i] = :dash
        lineCol[i] = prev_nz == 0 ? 1 : lineCol[prev_nz]+1
        prev_nz = i
        end
    end
    =#
    fd_jac = zeros(Float64,(nkx,nky,1,length(times)))
    for z in 1:nz
        fd_jac .+= abs.(fieldData.data[:,:,z,:,tsIdx:teIdx]).*diag.geom.jac[z]
    end
    plotData = fd_jac./sum(diag.geom.jac)

    plt = linePlot(times,plotData[modes[1][1],modes[1][2],1,:],"\$t \\quad(a/c_s)\$","\$|\\Phi|\$",:right,dataMin,dataMax,-Inf,Inf,"\$($(kx_modes[modes[1][1]]),$(ky_modes[modes[1][2]]))\$",true,lineStyle=lineSty[1],lineColor=lineCol[1])
    if length(modes)>1
        for i in 2:length(modes)
            plt = linePlot!(plt,times,plotData[modes[i][1],modes[i][2],1,:],"\$t \\quad(a/c_s)\$","\$|\\Phi|\$",:right,dataMin,dataMax,-Inf,Inf,"\$($(kx_modes[modes[i][1]]),$(ky_modes[modes[i][2]]))\$",true,lineStyle=lineSty[i],lineColor=lineCol[i])
        end
    end
    if logData
        plt = plot!(yaxis=:log10)
    elseif dataMin == -Inf
        plt = plot!(ylims=(0,dataMax))
    end
    return plt
end

function zonalResidual(diag,runID;timeStep::Int=-1)
    nz = diag.par_dicts[runID][:box][:nz0]
    fieldData = getFieldData(diag,runID)
    nt = timeStep == -1 ? length(fieldData.times) : timeStep
    phi_res = zeros(Float64,nt)
    phi_0 = 0.0
    for z in 1:nz
        jac = diag.geom.jac[z]
        phi_res .+= abs.(fieldData.data[2,1,z,1,:])*jac
        phi_0 += abs(fieldData.data[2,1,z,1,1])*jac
    end
    residual = phi_res./phi_0
    return residual
end



#=
Ballooning Diagnostic:
extendFieldData extends the field data along the field line using the nx0 connections and the
  parallel boundary condition Beer, Phys. Plasmas (1995).
getBallooningData outputs extended linear mode data along the field line at user indicated time
BallDiag outputs a plot of the ballooning mode data
=#
#NOT TESTED FOR Lx not adaptive or sign_Ip_CW or sign_Bt_CW
function extendFieldData(diag::DiagControl,runID::String)
    nz = diag.par_dicts[runID][:box][:nz0]
    nx = diag.par_dicts[runID][:box][:nx0]
    nx_even = iseven(nx)
    n_domains = nx-nx_even
    nx_mod = div(n_domains,2)
    ny = diag.par_dicts[runID][:box][:nky0]
    #q0 = diag.par_dicts[runID][:q0]
    n_pol = haskey(diag.par_dicts[runID],"n_pol") ? diag.par_dicts[runID][:n_pol] : 1
    n_fields = diag.par_dicts[runID][:info][:n_fields]
    shat = diag.par_dicts[runID][:geometry][:shat]
    kymin = diag.par_dicts[runID][:box][:kymin]
    Lx = diag.par_dicts[runID][:box][:lx]
    sign_Ip_CW = haskey(diag.par_dicts[runID][:geometry],"sign_Ip_CW") ? diag.par_dicts[runID][:geometry][:sign_Ip_CW] : 1
    sign_Bt_CW = haskey(diag.par_dicts[runID][:geometry],"sign_Bt_CW") ? diag.par_dicts[runID][:geometry][:sign_Bt_CW] : 1
    fieldData = getFieldData(diag,runID)
    nt = length(fieldData.times)

    field_ext = Array{Complex{Float64}}(undef,ny,nz*n_domains,n_fields,nt)
    ky_inds = 1
    sign_nexc = sign(shat)*sign_Ip_CW*sign_Bt_CW
    num_2pinpol_from_tube = [-nx_mod:nx_mod;]
    nexc = sign_nexc*(1 - diag.par_dicts[runID][:box][:adapt_lx]*(1-abs(shat)*kymin*Lx*ky_inds*n_pol))
    kx_prime_idx = vcat([nx-nx_mod+1:nx;],[1:nx_mod+1;])
    if sign(shat) == -1
        kx_prime_idx = reverse(kx_prime_idx)
    end

    phasefac = (-1.0+im*0.0)^nexc
    for j=1:n_domains
        field_ext[:,(j-1)*nz+1:j*nz,:,:] = phasefac^abs(num_2pinpol_from_tube[j]) .* fieldData.data[kx_prime_idx[j],:,:,:,:]
    end
    return field_ext
end

function getBallooningData(diag::DiagControl,runID::String;timeStart=0.0,timeEnd=-1,norm::Bool=false)
    fieldData = getFieldData(diag,runID)
    fieldData.data = extendFieldData(diag,runID)
    nkx = diag.par_dicts[runID][:box][:nx0]
    nky = size(fieldData.data,2)

    prof_data = Dict()
    timeEnd = timeEnd == -1 ? fieldDta.times[length(fieldData.times)] : timeEnd
    for i in 1:length(fieldData.times)
        time = fieldData.times[i]
        fd = fieldData.data[:,:,:,i]
        if time >= timeStart && time <= timeEnd
        prof_data[time] = fd
        end
    end

    ntimes = length(keys(prof_data))
    nkx_even = iseven(nkx)
    max_z = nkx-nkx_even
    nz_ext = max_z*diag.par_dicts[runID][:box][:nz0]
    dz = 2*max_z/nz_ext
    n_pol = haskey(diag.par_dicts[runID],"n_pol") ? diag.par_dicts[runID][:geometry][:n_pol] : 1
    z = n_pol.*[-max_z:dz:max_z-dz;]
    data = zeros(Complex{Float64},1,nz_ext,diag.par_dicts[runID][:info][:n_fields])
    for key in keys(prof_data)
        data .+= prof_data[key]./ntimes
    end
    data = data[1,:,:]./(1 .-norm*(1 .-data[1,div(nz_ext,2)+1,:]))

    return z,data
end

function BallDiag(diag::DiagControl,runID::String;ts=0.0,te=-1,titletext="?",whichData=[1,2,3])
    z,BallData = getBallooningData(diag,runID,timeStart=ts,timeEnd=te)
    plotData = zeros(Float64,(length(BallData),length(whichData)))
    BallLabels = fill("",length(whichData))

    ky = diag.par_dicts[runID][:box][:kymin]
    kx = haskey(diag.par_dicts[runID][:box], "kx_center") ? diag.par_dicts[runID][:box][:kx_center] : 0
    titletext = titletext=="?" ? "\$($kx,$ky)\$" : titletext
    if 1 in whichData
        plotData[:,1] = real.(BallData)
        BallLabels[1] = "Re\$\\Phi\$"
        if 2 in whichData
            plotData[:,2] = imag.(BallData)
            BallLabels[2] = "Im\$\\Phi\$"
            if 3 in whichData
                plotData[:,3] = abs.(BallData)
                BallLabels[3] = "\$|\\Phi|\$"
            end
        elseif 3 in whichData
        plotData[:,2] = abs.(BallData)
        BallLabels[2] = "\$|\\Phi|\$"
        end
    elseif 2 in whichData
        plotData[:,1] = imag.(BallData)
        BallLabels[1] = "Im\$\\Phi\$"
        if 3 in whichData
            plotData[:,2] = abs.(BallData)
            BallLabels[2] = "\$|\\Phi|\$"
        end
    elseif 3 in whichData
        plotData[:,1] = abs.(BallData)
        BallLabels[1] = "\$|\\Phi|\$"
    end
    if diag.plotType == "pyplot"
        fig,ax = linePlot(z,plotData,L"z/\pi","Amplitude",BallLabels,titletext=titletext)
        return fig,ax
    #else ... Cori compatible backend ...
    end
end


function computeFluctuationSpectrum(diag::DiagControl,runID::String)
    fieldData = getFieldData(diag,runID)
    nTimes = diag.dataFiles["field_"*runID].records
    nz0 = diag.par_dicts[runID][:box][:nz0]
    if nTimes != length(fieldData.times) 
      error("Inconsistent dimensions")
    end
  
    data_time_avg = sum(fieldData.data,dims=5)[:,:,:]/nTimes
    data_z_avg = sum(data_time_avg,dims=3)[:,:]/nz0
    return data_z_avg
end
=#

function ExB_velocity(diag::DiagControl{T},
                      run_ID::S;
                     ) where {T, S <: AbstractString}
end

function ExB_velocity_x!(u_ExB_array::AbstractArray{Complex{T}},
                         ϕ_array::AbstractArray{Complex{T}},
                         diag::DiagControl{T},
                         #run_ID::S,
                        ) where {T}
    (nx, ny, nz) = size(ϕ_array)
    kys = reshape(repeat(-im * diag.coords.ky, inner = nx, outer = nz), (nx, ny, nz))
    u_view = view(u_ExB_array, :, :, :)
    u_view[:, :, :] .= kys .* view(ϕ_array, :, :, :)
end

function B_x!(Bx_array::AbstractArray{Complex{T}},
              A_par::AbstractArray{Complex{T}},
              diag::DiagControl{T},
             ) where {T}
    (nx, ny, nz) = size(A_par)
    kys = reshape(repeat(im * diag.coords.ky, inner = nx, outer = nz), (nx, ny, nz))
    Bx_view = view(Bx_array, :, :, :)
    Bx_view[:, :, :] .= kys .* view(A_par, :, :, :)
end