function omegaSetup()
  return fileList = ["omega"]
end

function getNumEVsComp(diag::Diag,runID::String,n_ev::Int)
  key = "eigenvalues_"*runID
  file = diag.dataPath*"/"*key
  fStream = open(file,"r")
  fileLines = readlines(fStream)
  close(fStream)
  ev_comp = length(fileLines) - 1
  n_ev = n_ev == -1 ? haskey(diag.parDicts[runID],"n_ev") ? diag.parDicts[runID]["n_ev"][1] : 1  : n_ev
  n_ev = ev_comp < n_ev ? ev_comp : n_ev
  return ev_comp
end

function getLinearScanEigenvalues(diag::Diag)
#Only gets dominant growth rate right now. Will be updated to get all scan.log data later
  fileData = readdlm(diag.dataPath*"/scan.log")
  nkx = length(findall(x->x==fileData[2,3],fileData[:,3]))
  nky = length(findall(x->x==fileData[2,5],fileData[:,5]))
  kx = zeros(Float64,nkx)
  ky = zeros(Float64,nky)
  GammaDict = Dict()
  OmegaDict = Dict()
  for ev in 1:div(size(fileData,2)-5,3)
    gamma = zeros(Float64,(nky,nkx))
    omega = zeros(Float64,(nky,nkx))
    for i =2:size(fileData,1)
      kxidx = div(i-2,nky,RoundDown)+1
      kyidx = i-1-(kxidx-1)*nky
      kx[kxidx] = fileData[i,5]
      ky[kyidx] = fileData[i,3]
      gamma[kyidx,kxidx] = fileData[i,4+ev*3] == "" ? 0.0 : fileData[i,4+ev*3]
      omega[kyidx,kxidx] = fileData[i,5+ev*3] == "" ? 0.0 : fileData[i,5+ev*3]
    end
    GammaDict[ev] = gamma
    OmegaDict[ev] = omega
  end
  return kx,ky,GammaDict,OmegaDict
end

function getOmegaData(diag::Diag,runID::String)
  omegaData = OmegaData()
  key = "omega_"*runID
  omegaInfo = diag.dataFiles[key]
  T = omegaInfo.dataType
  file = diag.dataPath*"/"*key
  fStream = open(file,"r")
  fileLines = readlines(fStream)
  close(fStream)
  tempVals = Array{T}(undef,0)
  convertArrayFromString1D!(fileLines[1],T,tempVals)
  omegaData.kyps = tempVals[1]
  omegaData.gamma = isnan(tempVals[2]) ? 0.0 : tempVals[2]
  omegaData.omega = isnan(tempVals[3]) ? 0.0 : tempVals[3]
  return omegaData
end




function getEigenvalueData(diag::Diag,runID::String;n_ev::Int=-1)
  evalData = EigenvalueData()
  key = "eigenvalues_"*runID
  evalInfo = diag.dataFiles[key]
  T = evalInfo.dataType
  file = diag.dataPath*"/"*key
  fStream = open(file,"r")
  fileLines = readlines(fStream)
  close(fStream)
  n_ev = getNumEVsComp(diag,runID,n_ev)
  dataArr = Array{T}(undef,n_ev,2)
  tempVals = Array{T}(undef,0)
  for i in 1:n_ev
    convertArrayFromString1D!(fileLines[i+1],T,tempVals)
    dataArr[i,1] = isnan(tempVals[1]) ? 0.0 : tempVals[1]
    dataArr[i,2] = isnan(tempVals[2]) ? 0.0 : tempVals[2]
    tempVals = Array{T}(undef,0)
  end
  evalData.kyps = diag.parDicts[runID]["kymin"][1]
  evalData.gamma = dataArr[:,1]
  evalData.omega = dataArr[:,2]
  return evalData
end



function plotEigenvalueGrowthRates(diag;n_ev::Int=-1,legend_posEV::Symbol=:right,yminEV::Float64=-Inf,ymaxEV::Float64=Inf,xminEV::Float64=0.0,xmaxEV::Float64=Inf,titlesEV::String="",legendEV::Bool=false)
  num_runs = length(diag.runIDs)
  n_ev = n_ev == -1 ? diag.parDicts["0001"]["n_ev"][1] : n_ev
  kyps = Array{Float64}(undef,num_runs,1)
  gamma = Array{Float64}(undef,num_runs,n_ev)
  for i in 1:num_runs
    runID = diag.runIDs[i]
    ev_comp = getNumEVsComp(diag,runID,n_ev)
    evalData = getEigenvalueData(diag,runID,n_ev=ev_comp)
    kyps[i] = evalData.kyps
    evalData.gamma = length(evalData.gamma) != n_ev ? vcat(evalData.gamma,fill(NaN,Int(n_ev-ev_comp))) : evalData.gamma
    gamma[i,:] = evalData.gamma
  end
  plt = linePlot(kyps,gamma[:,1],"\$k_y\\rho_s\$","\$\\gamma \\quad (c_s/a)\$",legend_posEV,yminEV,ymaxEV,xminEV,xmaxEV,"\$\\gamma_1\$ $titlesEV",legendEV)
  if n_evs > 1
    for i in 2:n_evs
      linePlot!(plt,kyps,gamma[:,i],"\$k_y\\rho_s\$","\$\\gamma \\quad (c_s/a)\$",legend_posEV,yminEV,ymaxEV,xminEV,xmaxEV,"\$\\gamma_{$i}\$ $titlesEV",legendEV)
    end
  end
  plt = plot!([0.0],linetype=:hline,lc="black",label="")
  return plt
end

function plotEigenvalueGrowthRates!(plt,diag;n_evs::Int=-1,legend_posEV::Symbol=:right,yminEV::Float64=-Inf,ymaxEV::Float64=Inf,xminEV::Float64=0.0,xmaxEV::Float64=Inf,titlesEV::String="",legendEV::Bool=false)
  num_runs = length(diag.runIDs)
  n_evs = n_evs == -1 ? diag.parDicts["0001"]["n_ev"][1] : n_evs
  kyps = Array{Float64}(undef,num_runs,1)
  gamma = Array{Float64}(undef,num_runs,n_evs)
  for i in 1:num_runs
    runID = diag.runIDs[i]
    ev_comp = getNumEVsComp(diag,runID,n_ev)
    evalData = getEigenvalueData(diag,runID,n_ev=ev_comp)
    kyps[i] = evalData.kyps
    evalData.gamma = length(evalData.gamma) != n_ev ? vcat(evalData.gamma,fill(NaN,Int(n_ev-ev_comp))) : evalData.gamma
    gamma[i,:] = evalData.gamma
  end
  for i in 1:n_evs
    linePlot!(plt,kyps,gamma[:,i],"\$k_y\\rho_s\$","\$\\gamma \\quad (c_s/a)\$",legend_posEV,yminEV,ymaxEV,xminEV,xmaxEV,"\$\\gamma_{$i}\$, $titlesEV",legendEV)
  end
  return plt
end

function plotEigenvalueModes(diag::Diag,runIDs::Array{String};n_evs::Int=-1,yminEV::Float64=-Inf,ymaxEV::Float64=Inf,xminEV::Float64=0.0,xmaxEV::Float64=Inf,titlesEV::String="",legendEV::Symbol=:none,plttitle::String="")
  plotInit()
  num_runs = length(runIDs)
  n_evs = n_evs == -1 ? diag.parDicts[runIDs[1]]["n_ev"][1] : n_evs
  evalData = getEigenvalueData(diag,runIDs[1],n_ev=n_evs)
  if titlesEV == ""
    titlesEV = "\$k_y\\rho_s = $(evalData.kyps)\$"
  end
  plt = scatter(evalData.gamma,evalData.omega,label=titlesEV,legend=legendEV,grid=false,guidefontsize = 20, tickfontsize = 16,legendfontsize = 12, dpi = 500, size=(600,400), bottom_margin = 20px,left_margin=20px,xlabel="\$\\gamma \\quad (a/c_s)\$",ylabel="\$\\omega \\quad (a/c_s)\$",title=plttitle,titlefontsize=20,msw=0,ms=8)
  plt = plot!([0.0 0.0],linetype=[:hline :vline],lc=["black" "black"],label=["" ""])
  if num_runs>1
    for i in 2:num_runs
      evalData = getEigenvalueData(diag,runIDs[i],n_ev=n_evs)
      titlesEV = "\$k_y\\rho_s = $(evalData.kyps)\$"
      plt = scatter!(evalData.gamma,evalData.omega,label=titlesEV,msw=0,ms=8,mc=i)
    end
  end
  return plt
end



function plotInitialValueGrowthRates(diag::Diag; legend_posIV::Symbol=:right,yminIV::Float64=-Inf,ymaxIV::Float64=Inf,xminIV::Float64=-Inf,xmaxIV::Float64=Inf,titlesIV::String="",legendIV::Bool=false)
  num_runs = length(diag.runIDs)
  kyps = Array{Float64}(undef,num_runs,1)
  gamma = Array{Float64}(undef,num_runs,1)
  for i in 1:num_runs
    runID = diag.runIDs[i]
    omegaData = getOmegaData(diag,runID)
    kyps[i] = omegaData.kyps
    gamma[i] = omegaData.gamma
  end
  plt = linePlot(kyps,gamma,"\$k_y\\rho_s\$","\$\\gamma \\quad (c_s/L_{ref})\$",legend_posIV,yminIV,ymaxIV,xminIV,xmaxIV,titlesIV,legendIV)
  return plt
end

function ploctInitialValueGrowthRates!(diag::Diag,plt; legend_posIV::Symbol=:right,yminIV::Float64=-Inf,ymaxIV::Float64=Inf,xminIV::Float64=-Inf,xmaxIV::Float64=Inf,titlesIV::String="",legendIV::Bool=false)
  num_runs = length(diag.runIDs)
  kyps = Array{Float64}(undef,num_runs,1)
  gamma = Array{Float64}(undef,num_runs,1)
  for i in 1:num_runs
    runID = diag.runIDs[i]
    omegaData = getOmegaData(diag,runID)
    kyps[i] = omegaData.kyps
    gamma[i] = omegaData.gamma
  end
  plt = linePlot!(plt,kyps,gamma,"\$k_y\\rho_s\$","\$\\gamma \\quad (c_s/L_{ref})\$",legend_posIV,yminIV,ymaxIV,xminIV,xmaxIV,titlesIV,legendIV)
  return plt
end



function plotInitialValueRealFreq(diag::Diag; legend_posIV::Symbol=:right,yminIV::Float64=-Inf,ymaxIV::Float64=Inf,xminIV::Float64=-Inf,xmaxIV::Float64=Inf,titlesIV::Array{String}=[""],legendIV::Bool=false)
  num_runs = length(diag.runIDs)
  kyps = Array{Float64}(undef,num_runs,1)
  omega = Array{Float64}(undef,num_runs,1)
  for i in 1:num_runs
    runID = diag.runIDs[i]
    omegaData = getOmegaData(diag,runID)
    kyps[i] = omegaData.kyps
    omega[i] = omegaData.omega
  end
  plt = linePlot(kyps,omega,"\$k_y\\rho_s\$","\$\\omega \\quad (c_s/L_{ref})\$",legend_posIV,yminIV,ymaxIV,xminIV,xmaxIV,titlesIV,legendIV)
  return plt
end

function plotInitialValueRealFreq!(diag::Diag,plt; legend_posIV::Symbol=:right,yminIV::Float64=-Inf,ymaxIV::Float64=Inf,xminIV::Float64=-Inf,xmaxIV::Float64=Inf,titlesIV::Array{String}=[""],legendIV::Bool=false)
  num_runs = length(diag.runIDs)
  kyps = Array{Float64}(undef,num_runs,1)
  omega = Array{Float64}(undef,num_runs,1)
  for i in 1:num_runs
    runID = diag.runIDs[i]
    omegaData = getOmegaData(diag,runID)
    kyps[i] = omegaData.kyps
    omega[i] = omegaData.omega
  end
  plt = linePlot(kyps,omega,"\$k_y\\rho_s\$","\$\\omega \\quad (c_s/L_{ref})\$",legend_posIV,yminIV,ymaxIV,xminIV,xmaxIV,titlesIV,legendIV)
  return plt
end



function compareInitialValueGrowthRates(diagList::DiagList; legend_pos::Symbol=:topleft,ymin::Float64=-Inf,ymax::Float64=Inf,xmin::Float64=-Inf,xmax::Float64=Inf,titles::Array{String}=[""],legend_flag::Bool=false)
  @assert diagList.num_diags>1 "diagList must have more than one diag to compare!"
  list = collect(diagList.diagDict)
  if titles == [""]
    titles = Array{String}(undef,1,)
    for i in 1:diagList.num_diags
      titles[i] = list[i][1]
    end
  end
  plt = plotInitialValueGrowthRates(list[1][2]; legend_posIV=legend_pos,yminIV=ymin,ymaxIV=ymax,xminIV=xmin,xmaxIV=xmax,titlesIV=titles[1],legendIV=legend_flag)
  for i in 2:diagList.num_diags
    plt = plotInitialValueGrowthRates!(list[i][2],plt; legend_posIV=legend_pos,yminIV=ymin,ymaxIV=ymax,xminIV=xmin,xmaxIV=xmax,titlesIV=titles[i],legendIV=legend_flag)
  end
  return plt
end

function compareInitialValueRealFreq(diagList::DiagList; legend_pos::Symbol=:topleft,ymin::Float64=-Inf,ymax::Float64=Inf,xmin::Float64=-Inf,xmax::Float64=Inf,titles::Array{String}=[""],legend_flag::Bool=false)
  @assert diagList.num_diags>1 "diagList must have more than one diag to compare!"
  list = collect(diagList.diagDict)
  if titles == [""]
    titles = Array{String}(undef,1,)
    for i in 1:diagList.num_diags
      titles[i] = list[i][1]
    end
  end
  plt = plotInitialValueRealFreq(list[1][2]; legend_posIV=legend_pos,yminIV=ymin,ymaxIV=ymax,xminIV=xmin,xmaxIV=xmax,titlesIV=titles[1],legendIV=legend_flag)
  for i in 2:diagList.num_diags
    plt = plotInitialValueRealFreq!(list[i][2],plt; legend_posIV=legend_pos,yminIV=ymin,ymaxIV=ymax,xminIV=xmin,xmaxIV=xmax,titlesIV=titles[i],legendIV=legend_flag)
  end
  return plt
end



function plotkxkyGrowthRates(diag::Diag;cbarlims::Tuple{Float64,Float64}=(-Inf,Inf),which_ev::Int=1,mapCol="plasma",titletext="",levMin=-1)
  kx,ky,gamma,omega = getLinearScanEigenvalues(diag)
  plotData = gamma[which_ev]
  nan_idx = findall(x->x==1,isnan.(gamma[which_ev]))
  for i in 1:length(nan_idx)
    plotData[nan_idx[i]] = 0.0
  end
#=
  neg_idx = findall(x->x<0,plotData)
  for i in 1:length(neg_idx)
    plotData[neg_idx[i]] = 0.0
  end
=#
  fig,ax = plotSurface(kx,ky,plotData,titletext,"\$\\gamma\\quad(c_{\\Large\\textrm{s}}/a)\$","\$k_x\\rho_{\\Large\\textrm{s}}\$","\$k_y\\rho_{\\Large\\textrm{s}}\$",cbarlims=cbarlims,levelMin=levMin,mapcolor=mapCol)
  return fig,ax
end
