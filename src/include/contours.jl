
function Contours(diag::DiagControl{T},
                  var_list::Union{Symbol, Vector{Symbol}, NTuple{N, Symbol}} = (:phi,),
                  which_z::Union{Vector{S}, Vector{I}} = [div(length(diag.coords.z), 2) + 1],
                 ) where {T, N, S <: AbstractFloat, I <: Integer}

    vars = Tuple(unique(var_list isa Vector ? Tuple(var_list...) : Tuple(var_list)))
    n_vars = length(vars)
    n_spec = length(diag.active_species)
    nz = length(diag.coords.z)
    z_tuple = if which_z == [-1]
        z = collect(range(start = 1, stop = length(diag.coords.z)))
        nz_plot = 1
        (true, z)
    else
        nz_plot = length(which_z)
        (false, which_z)
    end 
    nx = length(diag.coords.x)
    ny = length(diag.coords.y)
    
    ax_args = map(i -> Expr(:kw, vars[i], i), eachindex(vars))
    ax = eval(Expr(:call, :(ComponentArrays.Axis), ax_args...))
    data = Array{Complex{T}, 3}(undef, nx, ny,nz)
    plot_data = eval(Expr(:call, :ComponentArray, map(i -> Expr(:kw, i, zeros(T, nx, ny, nz_plot, n_spec)), vars)...))
    return Contours{T, Tuple(diag.active_species), (:field, :mom)}(data, plot_data, vars, z_tuple)
end

function Contours()
    return Contours{Float64, (:i, :e), (:field, )}(ComponentArray(phi = Vector{Float64}()), (GeneTools.phi,), (true, false), (true, false))
end

function run_diag!(contour_diag::Contours{T, S, F},
                   diag::DiagControl{T},
                   timeslice_data::GeneTimesliceData{T, S, F},
                  ) where {T, S, F}
    contours!(contour_diag, diag, timeslice_data)
end

function contours!(contour_diag::Contours{T, S, F},
                   diag::DiagControl{T},
                   timeslice_data::GeneTimesliceData{T, S, F}
                  ) where {T, S, F}
    z_inds = contour_diag.z[1] ? [-1] : contour_diag.z[2]
    jacobian = diag.geom.jac / sum(diag.geom.jac) * length(diag.geom.jac)
    data_view = view(contour_diag.data, :, :, :)
    real_data = Array{T}(undef, size(contour_diag.data))
    for (j, s) in enumerate(species(contour_diag))
        for var in contour_diag.var_list
            data_view = if var in [:phi, :A_par, :B_par]
                filename = "field"
                view(timeslice_data.data[filename][var], :, :, :)
            else
                filename = "mom_"*string(s)
                view(timeslice_data.data[filename][var], :, :, :)
            end
            real_data .= diag.irfft_plan_xyz * data_view
            add_plot_data!(contour_diag, var, [:, :, :, j],
                           contour_slices(real_data, z_inds, jacobian))
        end
    end
end

function contour_slices(data::AbstractArray{T},
                        z_inds::AbstractVector{Int},
                        jacobian::AbstractVector{T}) where {T}
    which_z = z_inds == [-1] ? collect(axes(data, 3)) : z_inds
    if z_inds == [-1]
        res = Matrix{T}(undef, size(data)[1:2])
        for I in CartesianIndices(res)
            data_view = view(data, I[1], I[2], :)
            res[I] = sum(data_view .* jacobian)
        end
        return res / length(jacobian)
    else
        return data[:, :, which_z]
    end
end

function get_fields_and_moments(cont_diag::Contours{T, S, F}) where {T, S, F}
    return [cont_diag.var_list...]
end
