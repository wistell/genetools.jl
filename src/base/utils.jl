
struct CatmullRom{T}
    point::MVector{4, T}
    coeffs::MVector{4, T}
    mat::SMatrix{4, 4, T, 16}
end

function CatmullRom(T::Type = Float64)
    mat = @SMatrix [T(0.5) T(1.5) T(-1.5) T(0.5);
                    one(T) T(-2.5) T(2.0) T(-1.5);
                    T(-1.5) T(1.5) zero(T) zero(T);
                    zero(T) one(T) zero(T) zero(T)]
    return CatmullRom{T}(MVector{4, T}(zeros(T, 4)), MVector{4, T}(zeros(T, 4)), mat)
end

function (itp::CatmullRom)(x, data::AbstractVector)
    length(data) == 4 || error("Lenght of input data must be 4")
    itp.coeffs .= itp.mat * data
    map!(i -> x ^ i, itp.point, 3:-1:0)
    return dot(itp.coeffs, itp.point)
end