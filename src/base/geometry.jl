export GeneFromPest, PestFromGene, FluxFromGene, GeneFromFlux, GeneCoordinates, write_gene_geometry, gene_geometry_coefficients

struct GeneFromPest <: Transformation; end
struct GeneFromFlux <: Transformation; end
struct PestFromGene <: Transformation; end
struct FluxFromGene <: Transformation; end

struct GeneCoordinates{T, A} <: AbstractMagneticCoordinates
    x::T
    y::A
    z::A
end

function GeneCoordinates(x, y, z)
    x2, y2, z2 = promote(x, y, z)
    return GeneCoordinates{typeof(x2),typeof(y2)}(x2, y2, z2)
end

Base.show(io::IO, x::GeneCoordinates) = 
    print(io, "GeneCoordinates(x=$(x.x), y=$(x.y), z=$(x.z))")
Base.isapprox(x1::GeneCoordinates, x2::GeneCoordinates; kwargs...) =
    isapprox(x1.x, x2.x; kwargs...) &&
    isapprox(x1.y, x2.y; kwargs...) &&
    isapprox(x1.z, x2.z; kwargs...)

"""
  setGeometry!(diag::Diag)

Sets the geometry for the simulation.  Updates the geometry entry in the diag::Diag struct.
"""
function set_geometry!(diag::DiagControl)
  #geomKeys = ["magn_geometry", "q0", "shat", "geomdir", "geomfile", "minor_r", "major_R", "n_pol", "treps", "alpha", "dpdx_term", "dpdx_pm", "norm_flux_projection"]
  # TODO: implement geometry checks
  run = first(diag.run_IDs)
  geomType = diag.parameters[run][:geometry][:magn_geometry]
  # Set up the theta grid, which is the same as diag.grid.z
  diag.geom.theta = diag.coords.z

  # TODO: implement different geometries
  if geomType == "s_alpha"
    diag = geomSAlpha!(diag)
  end
  if geomType == "gist"
    diag = read_gist_file!(diag)
  end
  if geomType == "miller"
    diag = readMillerFile!(diag)
  end
  return diag
end

function set_geometry(geom_file::String,geom_type::String)
    #geomKeys = ["magn_geometry", "q0", "shat", "geomdir", "geomfile", "minor_r", "major_R", "n_pol", "treps", "alpha", "dpdx_term", "dpdx_pm", "norm_flux_projection"]
    # TODO: implement geometry checks
    # Set up the theta grid, which is the same as diag.grid.z

    geom = GeneGeom{Float64}()
    # TODO: implement different geometries
    if geom_type == "s_alpha"
        geom = geomSAlpha!(geom_file,geom)
    end
    if geom_type == "gist"
        geom = read_gist_file!(geom_file,geom)
    end
    if geom_type == "miller"
        geom = readMillerFile!(geom_file,geom)
    end
    
    return geom
end

"""
  read_gist_file!(diag::Diag)

Reads from a gist file from the first run in the series
"""
function read_gist_file!(diag::DiagControl)
    run = diag.run_IDs[1]
    # A gist file has different layout from a GENE file because there are no time records
    gistfile = "gist_"*run
    geomFile = isfile(joinpath(diag.data_path, gistfile)) ? joinpath(diag.data_path, gistfile) : throw(ArgumentError("gist_$(run) does not exist!"))
    nz = diag.parameters[run][:box][:nz0]
    geomData = Array{Float64}(undef, 16, nz)
    fStream = open(geomFile, "r")
    geomLines = readlines(fStream)
    close(fStream)
    header = true
    i = 0
    while header
        i += 1
        header = ~isequal(geomLines[i],"/")
    end
    for j = 1:nz
        tempVals = Array{Float64}(undef,0)
        string_vals = replace(strip(geomLines[i + j]), r"\s+" => " ")
        string_vals = replace(string_vals, " " => ", ")
        #convertArrayFromString1D!(geomLines[i+j],Float64,tempVals)
        geomData[:,j] .= eval(Meta.parse("["*string_vals*"]"))
    end
    # Fill in the appropriate entries from the gist file
    diag.geom.gxx = geomData[1, :]
    diag.geom.gxy = geomData[2, :]
    diag.geom.gyy = geomData[4, :]
    diag.geom.modB = geomData[7, :]
    diag.geom.jac = geomData[11, :]
    diag.geom.K1 = geomData[9, :]
    diag.geom.dBdz = geomData[10, :]
    diag.geom.K2 = geomData[8, :]
    return diag
end

function read_gist_file!(geom_file::String,geom::GeneGeom)
    # A gist file has different layout from a GENE file because there are no time records
    f_stream = open(geom_file, "r")
    geom_lines = readlines(f_stream)
    close(f_stream)
    header = true
    i = 0
    nz = 0
    n_pol = 1
    while header
        i += 1
        if occursin("gridpoints",geom_lines[i])
            nz = eval(Meta.parse(split(geom_lines[i],"=")[end]))
        end
        if occursin("n_pol",geom_lines[i])
            n_pol =  eval(Meta.parse(split(geom_lines[i],"=")[end]))
        end
        header = ~isequal(geom_lines[i],"/")
    end
    geom_data = Array{Float64}(undef, 16, nz)

    for j = 1:nz
        string_vals = replace(strip(geom_lines[i + j]), r"\s+" => " ")
        string_vals = replace(string_vals, " " => ", ")
        #convertArrayFromString1D!(geomLines[i+j],Float64,tempVals)
        geom_data[:,j] .= eval(Meta.parse("["*string_vals*"]"))
    end
    # Fill in the appropriate entries from the gist file
    geom.gxx = geom_data[1, :]
    geom.gxy = geom_data[2, :]
    geom.gyy = geom_data[4, :]
    geom.modB = geom_data[7, :]
    geom.jac = geom_data[11, :]
    geom.K1 = geom_data[9, :]
    geom.dBdz = geom_data[10, :]
    geom.K2 = geom_data[8, :]

    dz = 2*n_pol/nz
    geom.theta = [-n_pol:dz:n_pol-dz;]
    
    return geom
end

function readMillerFile!(diag::DiagControl)
    run = diag.run_IDs[1]
    # A gist file has different layout from a GENE file because there are no time records
    gistfile = "miller_"*run
    geomFile = isfile(joinpath(diag.data_path,gistfile)) ? joinpath(diag.data_path, gistfile) : throw(ArgumentError("miller_$(run) does not exist!"))
    nz = diag.parameters[run]["nz0"][1]
    geomData = Array{Float64}(undef,16,nz)
    fStream = open(geomFile,"r")
    geomLines = readlines(fStream)
    close(fStream)
    header = true
    i = 0
    while header
        i += 1
        header = ~isequal(geomLines[i],"/")
    end
    for j = 1:nz
        tempVals = Array{Float64}(undef,0)
        convertArrayFromString1D!(geomLines[i+j],Float64,tempVals)
        length(tempVals)
        geomData[:,j] = tempVals
    end
    # Fill in the appropriate entries from the gist file
    diag.geom.gxx = geomData[1,:]
    diag.geom.gxy = geomData[2,:]
    diag.geom.gyy = geomData[4,:]
    diag.geom.modB = geomData[7,:]
    diag.geom.jac = geomData[11,:]
    diag.geom.K1 = geomData[9,:]
    diag.geom.dBdz = geomData[10,:]
    diag.geom.K2 = geomData[8,:]
    return diag
end

function readMillerFile!(geom_file::String,geom::GeneGeom)
    # A gist file has different layout from a GENE file because there are no time records
    geom_data = Array{Float64}(undef,16,nz)
    fStream = open(geomFile,"r")
    geomLines = readlines(fStream)
    close(fStream)
    header = true
    i = 0
    while header
        i += 1
        header = ~isequal(geomLines[i],"/")
    end
    for j = 1:nz
        tempVals = Array{Float64}(undef,0)
        convertArrayFromString1D!(geomLines[i+j],Float64,tempVals)
        length(tempVals)
        geomData[:,j] = tempVals
    end
    # Fill in the appropriate entries from the gist file
    diag.geom.gxx = geomData[1,:]
    diag.geom.gxy = geomData[2,:]
    diag.geom.gyy = geomData[4,:]
    diag.geom.modB = geomData[7,:]
    diag.geom.jac = geomData[11,:]
    diag.geom.K1 = geomData[9,:]
    diag.geom.dBdz = geomData[10,:]
    diag.geom.K2 = geomData[8,:]
    return diag
end

"""
  geomSAlpha!(diag::Diag)

Constructs the s_alpha geometry for a GENE simulation
"""
function geomSAlpha!(diag::DiagControl)
  shat = diag.parameters[diag.run_IDs[1]]["shat"][1]
  q0 = diag.parameters[diag.run_IDs[1]]["q0"][1]
  trpeps = diag.parameters[diag.run_IDs[1]]["trpeps"][1]
  diag.geom.gxx = map(z->1,range(1,stop=length(diag.geom.theta)))
  diag.geom.gxy = map(z->shat*z,diag.geom.theta)
  diag.geom.gyy = map(z->1+(shat*z)^2,diag.geom.theta)
  diag.geom.modB = map(z->1/(1+trpeps*cos(z)),diag.geom.theta)
  diag.geom.jac = map(z->q0*(1+trpeps*cos(z)),diag.geom.theta)
  diag.geom.K2 = map(z->-(cos(z)+shat*z*sin(z))/(1+trpeps*cos(z))^2,diag.geom.theta)
  diag.geom.K1 = map(z->-sin(z)/(1+trpeps*cos(z))^2,diag.geom.theta)
  diag.geom.dBdz = map(z->-trpeps*sin(z)/(1+trpeps*cos(z))^2,diag.geom.theta)
  return diag
end

function extend_flux_tube(diag::DiagControl)
    shat = diag.parameters[diag.run_IDs[1]][:geometry][:shat]
    n_pol = Int(abs(minimum(diag.geom.theta)./pi))
    nz0 = diag.parameters[diag.run_IDs[1]][:box][:nz0]
    kymin = diag.parameters[diag.run_IDs[1]][:box][:kymin]
    nx = diag.parameters[diag.run_IDs[1]][:box][:nx0]

    max_theta = nx*n_pol  
    nPoints = max_theta*nz0
    nIntervals = Int((max_theta-1)/2)
    dz = 2*n_pol/nz0
    zeroIndex = Int(nPoints/2+1)

    theta_k_ext = Array{Float64}(undef,nPoints)
    gxx_ext = Array{Float64}(undef,nPoints)
    gxy_ext = Array{Float64}(undef,nPoints)
    gyy_ext = Array{Float64}(undef,nPoints)
    modB_ext = Array{Float64}(undef,nPoints)
    jac_ext = Array{Float64}(undef,nPoints)
    K2_ext = Array{Float64}(undef,nPoints)
    K1_ext = Array{Float64}(undef,nPoints)
    dBdz_ext = Array{Float64}(undef,nPoints)

    gxx = diag.geom.gxx
    gxy = diag.geom.gxy
    gyy = diag.geom.gyy
    modB = diag.geom.modB
    jac = diag.geom.jac
    K2 = diag.geom.K2
    K1 = diag.geom.K1
    dBdz = diag.geom.dBdz

    for j in [nIntervals:-1:1;]
        theta_k = -2*n_pol*j
        for i = 1:nz0
        k = Int(zeroIndex + i-1 - nz0/2 + theta_k*nz0/2)
        theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i] + shat*theta_k*pi*gxx[i]
        gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi*gxy[i] + (shat*theta_k*pi)^2*gxx[i]
        modB_ext[k] = modB[i]
        jac_ext[k] = jac[i]
        K2_ext[k] = K2[i] + shat*theta_k*pi*K1[i]
        K1_ext[k] = K1[i]
        dBdz_ext[k] = dBdz[i]
        end
    end

    for i = 1:nz0
        k = Int(zeroIndex + i-1 - nz0/2)
        theta_k_ext[k] = (i-1 - nz0/2)*dz
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i]
        gyy_ext[k] = gyy[i]
        modB_ext[k] = modB[i]
        jac_ext[k] = jac[i]
        K2_ext[k] = K2[i]
        K1_ext[k] = K1[i]
        dBdz_ext[k] = dBdz[i]
    end

    for j = 1:nIntervals
        theta_k = 2*n_pol*j
        for i = 1:nz0
        k = Int(zeroIndex + i-1 - nz0/2 + theta_k*nz0/2)
        theta_k_ext[k] = (i-1 - nz0/2)*dz + theta_k
        gxx_ext[k] = gxx[i]
        gxy_ext[k] = gxy[i] + shat*theta_k*pi*gxx[i]
        gyy_ext[k] = gyy[i] + 2*shat*theta_k*pi*gxy[i] + (shat*theta_k*pi)^2*gxx[i]
        modB_ext[k] = modB[i]
        jac_ext[k] = jac[i]
        K2_ext[k] = K2[i] + shat*theta_k*pi*K1[i]
        K1_ext[k] = K1[i]
        dBdz_ext[k] = dBdz[i]
        end
    end
    #=
    diag.geom.gxx = gxx_ext
    diag.geom.gxy = gxy_ext
    diag.geom.gyy = gyy_ext
    diag.geom.modB = modB_ext
    diag.geom.jac = jac_ext
    diag.geom.K2 = K2_ext
    diag.geom.K1 = K1_ext
    diag.geom.dBdz = dBdz_ext
    =#
    return GeneGeom(gxx_ext,gxy_ext,gyy_ext,modB_ext,jac_ext,K2_ext,K1_ext,dBdz_ext)
end

function Ba end

function s0 end

function iota end

function shat end

function R_major end

function A_minor end

function dpdx end

function gene_geometry_coefficients end

function write_gene_geometry(filename::String,
                             coords,
                             eq::MG,
                             g::AbstractVector{SVector{6,T}},
                             modB::AbstractVector{T},
                             jac::AbstractVector{T},
                             K1::AbstractVector{T},
                             K2::AbstractVector{T},
                             dBdθ::AbstractVector{T}) where {T, MG <: AbstractGeometry}
    Ba = GeneTools.Ba(eq)
    geneFile = filename*".dat"
    io = Base.open(geneFile,"w")
    # Write the parameters namelist
    s0 = GeneTools.s0(eq,coords)
    α0 = getfield(first(coords),2)
    coordString = "!PEST coordinates\n"
    q0 = 1.0/GeneTools.iota(eq)
    shat = GeneTools.shat(eq,coords)
    lengthString = "!major, minor radius[m] = "*string(GeneTools.R_major(eq))*" "*string(GeneTools.A_minor(eq))*"\n"
    Bstring = "!Bref = "*string(Ba)*"\n"
    gridString = "gridpoints = "*string(length(coords))*"\n"
    npolString = "n_pol = "*string(abs(round(Int,getfield(last(coords),3)/q0/π)))*"\n"
    dpdx_string = "my_dpdx = "*string(GeneTools.dpdx(eq,coords))*"\n"

    write(io,"&parameters\n")
    write(io,coordString)
    write(io,"!s0, alpha0 = "*string(s0)*" "*string(α0)*"\n")
    write(io,lengthString)
    write(io,Bstring)
    write(io,dpdx_string)
    write(io,"q0 = "*string(q0)*"\n")
    write(io,"shat = "*string(shat)*"\n")
    write(io,gridString)
    write(io,npolString)
    write(io,"!sign_Ip_CW = "*string(convert(Int,sign(getfield(first(coords),1))))*"\n")
    write(io,"!sign_Bt_CW = "*string(convert(Int,sign(getfield(first(coords),1))))*"\n")
    write(io,"/\n")

    for i = 1:length(coords)
        s = @sprintf "%23.16f %23.16f %23.16f %23.16f %23.16f %23.16f %23.16f %23.16f %23.16f\n" g[i][1] g[i][2] g[i][3] modB[i] jac[i] K2[i] K1[i] dBdθ[i] 0.0
        write(io,s)
    end
    close(io)
end

