import Base: error
using FortranFiles


function binary_file_seek!(file_stream::FortranFile,
                           seek_position::Int64;
                          )
    # Reset the file to the head state
    rewind(file_stream)
    if seek_position > 1
        for i=1:seek_position-1
            # read(stream) simply advances the record without reading data
            read(file_stream)
        end
    end
    return nothing
end

"""
   read_gene_file_part!(file::String,dataArr::Array,tStep:Int64,fInd::Int64,nFields::Int64)

Reads one entry from a GENE binary data file and inserts into a pre-allocated array
* file: The absolute path to the file
* dataArr: The pre-allocated data array to be modified
* tStep: The timestep of the entry to be read
* fInd: The field index to be read
* nFields: The number of total fields per timestep
"""
function read_gene_file_part!(data_array::Array{T},
                              file::S,
                              time_step::Int,
                              field_index::Int,
                              n_fields::Int;
                             ) where {T, S <: AbstractString}
  file_stream = FortranFile(file)

  seek_position = (time_step - 1) * n_fields + 1 + field_index
  binary_file_seek!(file_stream, seek_position)
  read(file_stream, data_array)

  close(file_stream)
end


"""
    read_gene_text-file!(data_arr, gene_file, rec_locs, rec_fields; find_times = false)

Read a GENE text file with records given by `rec_locs` inserting
results into `data_arr`.
"""
function read_gene_nrg_file!(data_arr::Array{T},
                             gene_file::GeneFileInfo{T},
                             step_locs::Vector{Int},
                             rec_fields::Array{Int};
                             find_times = false,
                            ) where {T}
    #n_records = ndims(step_locs) == 2 ? size(step_locs,1) : throw(ArgumentError("Incorrect dimensions of $(:recLocs)"))
    n_records = length(step_locs)
    file = isfile(gene_file.filename) ? gene_file.filename : throw(ArgumentError("File $(gene_file.filename) does not exist"))
    file_stream = open(file, "r")
    file_lines = readlines(file_stream)
    close(file_stream)

    block_dims = gene_file.block_dims
    block_size = block_dims[end]
    time = Array{T}(undef, n_records)
    data_stride = block_dims[begin]
    block_stride = prod(block_dims)
    for (i, rec) in enumerate(step_locs)
        #if useTime[tInd]
        rec_line = (rec - 1) * (block_size + 1) + 1
        #temp_vals = Array{T}(undef,0)
        temp_vals = Meta.parse(file_lines[rec_line])
        time[i] = only(temp_vals)
        if find_times === false
            for j = 1:block_size
                temp_vals = eval(Meta.parse("["*replace(strip(file_lines[rec_line+j]), r" [\s]?(-)?" => s", \1")*"]"))
                start_loc = (i - 1) * block_stride + (j - 1) * data_stride + 1
                copyto!(data_arr, start_loc, temp_vals, 1, data_stride)
            end
        end
        #end
    end
    return time
end


"""
    read_gene_binary!(data_array::Array{Complex{T}}, gene_file_info::GeneFileInfo{T}, read_locs::Vector{Int}, field_locs::Union{Vector{Int}, Vector{Enum{I}}}; find_times = false)
Function that reads in Gene binary data.
All GENE simulation files will have the format
# time1
# data1
# time2
# data2
  .
  .
  .

The size of the data blocks between times is given by `gene_file_info.block_dims`,
while the size of the data records within each block are given by `gene_file_info.data_dims`.
The function inserts the data into the pre-allocated array `data_arr``. 
The record locations to be read from the file and the locations to be written to 
in `data_array` are given by the vector `read_locs` and must have entries that are
multiples of `gene_file_info.istep` + 1.  The data fields within each data block 
to be read are given by the `field_locs`` vector.
"""
function read_gene_binary!(data_array::AbstractArray{Complex{T}},
                           gene_file_info::GeneFileInfo{T},
                           read_locs::Vector{Int},
                           field_locs::Union{Vector{Int}, GeneIDVector};
                           find_times = false,
                          ) where {T}
    file_stream = if gene_file_info.access == "sequential"
        FortranFile(gene_file_info.filename, convert = gene_file_info.endian)
    elseif gene_file_info.access == "direct"
        FortranFile(gene_file_info.filename, access="direct", recl = 8)
    else
        error("$(getfield(gene_file_info,:filename)) requires access specification")
    end
    # Check for argument errors
    n_records = length(read_locs)
    n_fields = length(field_locs)
    data_dims = gene_file_info.data_dims
    data_stride = prod(data_dims)
    data_block =Array{Complex{T}}(undef, Tuple(data_dims))
    times = Array{T}(undef, n_records)

    for rec in eachindex(read_locs)
        times[rec] = read_gene_timeslice_binary!(data_block, file_stream, gene_file_info, read_locs[rec], field_locs)
        start_loc = n_fields * (rec - 1) * data_stride + 1
        copyto!(data_array, start_loc, data_block, 1, data_stride)
    end
    close(file_stream)
    return times
end

function read_gene_timeslice_binary!(data_array::ComponentArray,
                                     file_stream::FortranFile,
                                     gene_file_info::GeneFileInfo{T},
                                     read_loc::Int,
                                     field_locs::Union{Vector{Int}, GeneIDVector};
                                    ) where {T}

    data_dims = gene_file_info.data_dims
    data_stride = prod(data_dims)
    block_dims = gene_file_info.block_dims
    block_stride = prod(block_dims)
    time = zero(T)
    keys = propertynames(data_array)
    data_block = Array{Complex{T}}(undef, data_dims...)
    Cinds = CartesianIndices((1:data_dims[1], 1:data_dims[2], 1:data_dims[3]))
    if gene_file_info.access == "sequential"
        seek_record = div(read_loc - 1, gene_file_info.istep)
        seek_position_timestep = seek_record * (block_dims[end] + 1) + 1
        @debug "Reading record $seek_position_timestep"
        binary_file_seek!(file_stream, seek_position_timestep)
        time = read(file_stream, T)
        for (i, f) in enumerate(field_locs)
            # Find the indices of the array to be written into
            start_loc = (i - 1) * data_stride + 1
            seek_position_field = seek_position_timestep + Int(f)
            @debug start_loc, seek_position_field, keys[i]
            binary_file_seek!(file_stream, seek_position_field)
            read(file_stream, data_block)

            copyto!(data_array[keys[i]], Cinds, data_block, Cinds)
        end
    elseif gene_file_info.access == "direct"
        #= In direct access mode, one needs to read in each number at a time
        # For complex numbers, this means that the real part and the imaginary part will require reading two consecutive records
        =#
        seek_position_timestep = (block_stride + 1) * (read_loc - 1) + 1
        time = read(file_stream, rec = seek_position_timestep, T)
        write_location = read_loc
        for f in field_locs
            seek_position_field = seek_position_timestep + data_stride*(Int(f)-1)
            for j = 1:data_stride
                data_write_loc = (write_location - 1) * block_stride + (Int(f) - 1) * data_stride + j
                # Deal with complex entries
                if T <: Complex
                    seek_position_data = seek_position_field + 2 * (j - 1) + 1
                    real_val = read(file_stream,rec=seek_position_data, T)
                    imag_val = read(file_stream,rec=seek_position_data + 1, T)
                    data_array[data_write_loc] = T(real_val, imag_val)
                else
                    seek_position_data = seek_position_field + j
                    data_array[data_write_loc] = read(file_stream, rec=seek_position_data, T)
                end
            end
        end
    end
    return time
end

"""
    get_gene_data(gene_file_info::GeneFileInfo, read_locs::Vector{Int}, field_locs::Union{Vector{Int}, Vector{Enum}})

Retrieve the data from the file with metadata recorded in `gene_file_info`
at the local simulation timesteps given by `read_locs` for the field(s)
specified by field_locs. The `read_locs` vector must have entries that are
multiples of `gene_file_info.istep` + 1.

### Example
```julia-repl
julia> field_info = diag.file_info["run_ID"]; # For a GENE field file

julia> field_info.istep
100

julia> read_locs = [1, 101, 201];

julia> data = get_gene_data(field_info, read_locs, [1]); # Extract only the electrostatic potential

julia> typeof(data)
GeneData{Float64}
```
"""
function get_gene_data(gene_file_info::GeneFileInfo{T},
                       read_locs::Vector{Int},
                       field_locs::Union{Vector{Int}, GeneIDVector},
                      ) where {T}
    n_times = length(read_locs)
    data_array = Array{Complex{T}}(undef, gene_file_info.data_dims..., n_times)
    data_times = Vector{T}(undef, n_times)
    get_gene_data!(data_array, data_times, gene_file_info, read_locs, field_locs)
    return GeneData{T}(data_times, data_array)
end

"""
    get_gene_data!(data_array::Array{Complex{T}}, data_times::Vector{T}, gene_file_info::GeneFileInfo{T}, read_locs::Vector{Int}, field_locs::Vector{Int})
    get_gene_data!(data_array::Array{Complex{T}}, gene_file_info::GeneFileInfo{T}, read_locs::Vector{Int}, field_locs::Vector{Int})

Retrieve the data from the file with metadata recorded in `gene_file_info`
at the local simulation timesteps given by `read_locs` for the field(s)
specified by field_locs, inserting the results into `data_array` and `data_times`. 
The `read_locs` vector must have entries that are multiples of `gene_file_info.istep` + 1.

### Example
```julia-repl
julia> field_info = diag.file_info["run_ID"]; # For a GENE field file

julia> field_info.istep
100

julia> read_locs = [1, 101, 201];

julia> data_array = Array{ComplexF64}(undef, field_info.data_dims, 3);

julia> data_times = Vector{Float64}(undef, 3);

julia> get_gene_data!(data_array, data_times field_info, read_locs, [1]); # Extract only the electrostatic potential
"""
function get_gene_data!(data_array::Array{Complex{T}},
                        data_times::Vector{T},
                        gene_file_info::GeneFileInfo{T},
                        read_locs::Vector{Int},
                        field_locs::Union{Vector{Int}, GeneIDVector},
                       ) where {T}
    last(size(data_array)) == length(data_times) || throw(DimensionMismatch("Number of time slices in data array does not match time array"))
    times = read_gene_binary!(data_array, gene_file_info, read_locs, field_locs)
    copyto!(data_times, times)
    return nothing
end

function get_gene_data!(data_array::Array{Complex{T}},
                        gene_file_info::GeneFileInfo{T},
                        read_locs::Vector{Int},
                        field_locs::Union{Vector{Int}, GeneIDVector},
                       ) where {T}
    last(size(data_array)) == length(read_locs) || throw(DimensionMismatch("Number of time slices in data array does not match time array"))
    read_gene_binary!(data_array, gene_file_info, read_locs, field_locs)
    return nothing
end
