import FastGaussQuadrature: gausslegendre, gausslaguerre
export make_real_space_flux_tube_grid, mapGeneValuesToLabSpaceGrid!


struct FluxTube{T}
   gridpoints::Array{SVector{3, T}, 3}
   vals::Array{Complex{T}, 3}
   s::Real
   α0::Real
   ρ::Float64
end


function set_grid_coords!(diag::DiagControl{T}) where {T}
    # This function assumes the parameters files have all been checked to be consistent
    run = first(diag.run_IDs)

    lx = haskey(diag.parameters[run][:box],:lx) ? diag.parameters[run][:box][:lx] : diag.parameters[run][:info][:lx]
    ly = diag.parameters[run][:info][:ly]
    nkx = diag.parameters[run][:box][:nx0]
    kymin = diag.parameters[run][:box][:kymin]
    nky = diag.parameters[run][:box][:nky0]
    nz = diag.parameters[run][:box][:nz0]
    nv = diag.parameters[run][:box][:nv0]
    lv = diag.parameters[run][:box][:lv]
    nw = diag.parameters[run][:box][:nw0]
    lw = diag.parameters[run][:box][:lw]

    is_nonlinear = diag.parameters[first(diag.run_IDs)][:general][:nonlinear]

    kxmin = 2*pi/lx
    #kx = vcat(map(i->kxmin*i,range(0,stop=div(nkx,2)-1)),0,reverse(map(i->-kxmin*i,range(1,stop=div(nkx,2)-1))))
    kx = zeros(T, nkx)
    for i in 2:(div(nkx, 2) + 1)
        kx[i] = (i - 1) * kxmin
        kx[nkx - i + 2] = -kx[i]
    end
    x = collect(range(start = -0.5 * lx, step = lx / (nkx - 1), length = nkx))
    ky = kymin * collect(1 - is_nonlinear:1:nky) 
    y = collect(range(start = -0.5 * ly, step = ly / (2 * nky - 1), length = (2 * nky)))
    n_pol = haskey(diag.parameters[run][:box],:n_pol) ? diag.parameters[run][:box][:n_pol] : 1
    nz0 = div(nz, n_pol)
    dz = 2π/nz0
    z = map(i->-n_pol*π + dz*i,range(0,stop=nz-1))
    vx = map(i->-lv + 2*lv/(nv-1)*i,range(0,stop=nv-1))
    vw = fill(2*lv/(nv-1),nv)
    # Alternative Simpson's rule (taken from GENE)
    vw[1] = 2*lv/(nv-1)*17/48
    vw[2] = 2*lv/(nv-1)*59/48
    vw[3] = 2*lv/(nv-1)*43/48
    vw[4] = 2*lv/(nv-1)*49/48
    for i=0:3
        vw[nv-i] = vw[i+1]
    end
    mux, muw = get_mu_weights_and_knots(lw, nw,:gaulag)
    vpar = hcat(vx,vw)
    mu = hcat(mux, muw)
    diag.coords = GeneGrid{diag.parameters[run][:info][:PRECISION]}(kx, ky, x, y, z, vpar, mu)
    return diag
end
#=
function getVelSpaceGrid(diag::Diag)

end
=#

# The function gausslaguerre() from FastGaussQuadrature returns the same values as getGaussLaguerreWeights, therefore we will use that function to compute the mu weights and knots
function get_mu_weights_and_knots(lw::Float64,
                                  n::Int,
                                  GridType::Symbol;
                                 )
   if GridType === :gaulen
      knots, weights = gausslegendre(n)
   elseif GridType === :gaulag
      knots, weights = gausslaguerre(n)
      # This is what is done for the mu grid in GENE
      weights = map(i->weights[i]*exp(knots[i]),range(1,stop=length(weights)))
      fac = lw/sum(weights)
      weights *= fac
      knots *= fac
   end
   return knots, weights
end

# This routine is taken from GENE, need to properly attribute the source code
function getGaussLaguerreWeights(xu::T,
                                 n::Int;
                                ) where {T}
    x = Array{T}(undef,n)
    w = Array{T}(undef,n)

    hn = 1/n
    pf = 0
    pd = 0
    for nr=1:n
        z= nr > 1 ? x[nr-1]+hn*nr^1.27 : hn
        it=0
        z0=z
        while it <= 100 || abs(z-z0)/z <= 1e-15
            it+=1
            p=1.0
            for i=1:nr-1
                p*=(z-x[i])
            end
            f0=1
            f1=1-z
            for k=2:n
                pf=((2.0*k-1.0-z)*f1-(k-1.0)*f0)/k
                pd=k/z*(pf-f1)
                f0=f1
                f1=pf
            end
            fd=pf/p
            q=0.0
            for i=1:nr-1
                wp=1.0
                for j=1:nr-1
                    if j != i
                        wp*=(z-x[j])
                    end
                end
                q=q+wp
            end
            gd=(pd-q*fd)/p
            z=z-fd/gd
        end
        x[nr]=z
        w[nr]=1.0/(z*pd*pd)
    end
    w = map(i->w[i]*exp(x[i]),range(start = 1, stop = length(w)))
    fac = xu/sum(w)
    x=x*fac
    w=w*fac
    return x, w
end

function make_real_space_flux_tube_grid end


"""
This function maps gene coordinates onto real space, along with a chosen
fluctuation amplitude
Example:
using GeneTools, NetCDF, VMEC

wout = NetCDF.open("/home/gwheld/research/gradResearch/W7X/W7Xgeomfiles/EIM");
vmec= VMEC.readVmecWout(wout);
diag = GeneTools.Diag()
diag = GeneTools.startGeneDiag()
GeneTools.readDiagFile!(diag,"/home/gwheld/research/gradResearch/diag.in")
runID = "5" #whatever the run number of your file is
parDict = diag.parDicts[runID]
s = 0.16
α0 = 0.0
momData = GeneTools.getMomData(diag,"5","electrons",320.0,320.0,1).data[:,:,:,1,1];
fluxTube = makeRealSpaceFluxTubeGrid(s,α0,parDict,vmec);
fluxTube = GeneTools.mapGeneDataToRealSpace(momData,fluxTube);
"""
function mapGeneValuesToLabSpaceGrid!(fluxTube::FluxTube{T},
                                      data::Array{Complex{T}, 3};
                                     ) where {T}
    for i in axes(fluxTube.gridpoints, 1)
        realSpaceData = ifft(data[:,:,i])
        for j in axes(fluxTube.gridpoints, 2)
            for k in axes(fluxTube.gridpoints, 3)
                fluxTube.vals[i,j,k] = realSpaceData[j,k]
            end
        end
    end
    return fluxTube
end